# -*- coding: utf-8 -*-
########################
# Created on 10/9/2014
#
# @author: openerp
########################

import openerp
import logging

from openerp.osv import fields,orm,osv
from openerp.tools.translate import _

import base64
from StringIO import StringIO

_logger = logging.getLogger(__name__)

_REF = 0
_QTY = 1

class upload_stock(osv.osv_memory):
    '''
    Upload a file with sales result for a selling campaign
    '''

    _name = 'upload.stock'
    _columns = {
        'stock_file': fields.binary('Stock file'),
        'skip_first': fields.boolean('Skip first line'),
        'use_old_reference': fields.boolean('Use old reference?'),
        'separator': fields.selection(((',', ','),
                                       (';', ';'),
                                       ('|', '|')), 'Separator'),
    }
    
    
    def upload_stock(self, cr, uid, ids, context=None):
        res = self.read(cr, uid, ids,['stock_file', 'skip_first', 'use_old_reference', 'separator'])
        res = res and res[0] or {}
        warehouse = self.pool.get('supplier.warehouse').browse(cr, uid, context.get('warehouse'))
        w_line_obj = self.pool.get('supplier.warehouse.line')
        product_obj = self.pool.get('product.product')
        _skip = res['skip_first']
        _ref = 'default_code' if res['use_old_reference'] else 'product_code'
        _warning = '' 
        for row in StringIO(base64.decodestring(res['stock_file'])):
            if _skip == True:
                _skip = False
                continue
            fields = row.split(res['separator'].encode('utf-8'))
            p_id = product_obj.search(cr, uid, [(_ref,'=', fields[_REF])])
            if p_id:
                p_id = p_id[0]
                w_line = w_line_obj.search(cr, uid, [('warehouse_id', '=', warehouse.id), ('product', '=', p_id)])
                if w_line:
                    w_line = w_line_obj.browse(cr, uid, w_line)
                    w_line.write({'qty': int(w_line.qty) + int(fields[_QTY])})
                else:
                    w_line_obj.create(cr, uid, {'product': p_id, 'qty': fields[_QTY], 'warehouse_id': warehouse.id})
            else:
                _warning += 'Product with {0}: {1} not found.\n'.format('internal reference' if res['use_old_reference'] else 'product code', fields[_REF])
            
        if len(_warning) > 0:
            raise osv.except_osv(
                         _('Warning!'),
                         _(_warning))
upload_stock()