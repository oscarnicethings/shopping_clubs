# -*- coding: utf-8 -*-
########################
# Created on 22/8/2014
#
# @author: openerp
########################

import openerp
import logging

from openerp import netsvc, tools, pooler
from openerp.osv import fields, orm, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

import csv
import base64
import os
import time
from cStringIO import StringIO

import collections

from tools import convert
from tools import zipdir
import shutil

_logger = logging.getLogger(__name__)

class campaigns_clubs_rel(orm.Model):
    '''
    Relation between a campaign and a shopping club.
    '''
    
    _name = 'campaigns.clubs.rel'
    #_rec_name = 'id_campaign'
    _inherit = ['mail.thread']
    
    _clients_account_id = 443
    _vat_id = 1
    _spain_id = 69
    
    _date_format = '%Y-%m-%d'
    _to_read = [
            'product_code',
            'default_code',
            'name',
            'ean13',
            'description',
            'description_en',
            'lst_price',
            'campaign_price',
            'volume',
            'weight',
            'weight_net',
            'packing_width',
            'packing_height',
            'packing_length',
            'width',
            'height',
            'length',
        ]
    
    def _get_reference(self, cr, uid, ids, field, arg, context=None):
        # _logger.info('ids are {0}'.format(ids))
        return {ids[0]: 'SC{0:010d}'.format(ids[0])}
        
    def _get_selection(self, cr, uid, context=None):
        return (
            ('pendent', 'Pendent'),
            ('on', 'Active'),
            ('in_production', 'In production'),
            ('finished', 'Finished'))    
    
    def _get_campaign_id(self, cr, uid, ids, context=None):
        if isinstance(ids, list):
            ccrel_id = ids[0]
        elif isinstance(ids, int):
            ccrel_id = ids
        else:
            return None
        
        _c_id = self.read(cr, uid, ccrel_id, ['id_campaign'])['id_campaign']
        if _c_id:
            return _c_id[0]
        else:
            return False
    
    def _get_club_id(self, cr, uid, ids, context=None):
        return self.read(cr, uid, ids[0], ['id_club'])['id_club'][0]
    
    def _get_country_id(self, cr, uid, ids, context=None):
        return self.read(cr, uid, ids[0], ['id_country'])['id_country'][0]
        
    def _get_products_list(self, cr, uid, ids, context=None):
        
        _pc = self.pool.get('campaign').read(cr, uid, ids, ['campaign_products'])
        return _pc[0]['campaign_products'] if isinstance(_pc, list) else _pc['campaign_products']
        
    def _get_products(self, cr, uid, ids, context=None):
        _ids = []
        for line in self._get_lines(cr, uid, ids, context):
            _ids.append(line.product.id)
        return _ids
        
    def _get_lines(self, cr, uid, ids, context=None):
        _lines = self.browse(cr, uid, ids).sales_lines
        return _lines

    def _get_invoice(self, cr, uid, ids, context=None):
        try:
            return self.read(cr, uid, ids[0], ['invoice'])['invoice'][0]
        except:
            return []
        
    def _get_sale_order(self, cr, uid, ids, context=None):
        try:
            return self.read(cr, uid, ids[0], ['sale_order'])['sale_order'][0]
        except:
            return []
        
    def _get_invoiced_products(self, cr, uid, ids, context=None):
        _line_obj = self.pool.get('account.invoice.line')
        _invoice_id = self._get_invoice(cr, uid, ids, context)
        if _invoice_id:
            _line_ids = _line_obj.search(cr, uid, [('invoice_id', '=', _invoice_id)])
            _lines = _line_obj.read(cr, uid, _line_ids, ['product_id'])

            _formated_lines = []
            _formated_ids = []
            for _l in _lines:
                _formated_lines.append(_l['product_id'][0])
                _formated_ids.append(_l['id'])
        
        return (_formated_lines, _formated_ids)
    
    def _get_ordered_products(self, cr, uid, ids, context=None):
        _line_obj = self.pool.get('sale.order.line')
        _saleorder_id = self._get_sale_order(cr, uid, ids, context)
        if _saleorder_id:
            _line_ids = _line_obj.search(cr, uid, [('order_id', '=', _saleorder_id)])
            _lines = _line_obj.read(cr, uid, _line_ids, ['product_id'])

            _formated_lines = []
            _formated_ids = []
            for _l in _lines:
                _formated_lines.append(_l['product_id'][0])
                _formated_ids.append(_l['id'])
            
        return (_formated_lines, _formated_ids)
    
    def _get_order_amount(self, cr, uid, ids, field, arg, context=None):
        res = {}
        _total = 0.0
        _sls = self.browse(cr, uid, ids[0]).sales_lines
        for _sl in _sls:
            _total += _sl.campaign_price * _sl.qty
        
        res[ids[0]] = '{0:.2f}'.format(_total) 
        
        return res
    
    def _get_total_units(self, cr, uid, ids, field, arg, context=None):
        this = self.browse(cr, uid, ids[0])
        return {ids[0]: sum([line.qty for line in this.sales_lines])}
        
    def _get_sp_discount(self, cr, uid, ccrel=None, context=None):
        
        if ccrel is None:
            return 0
        
        if not self.browse(cr, uid, ccrel).id_club.shopping_club_category:
            return {'warning': {
                'title': _('Warning!'),
                'message' : _('This shopping club has no category assigned.\nPlease assign one.')
            }}
            
        return self.browse(cr, uid, ccrel).id_club.shopping_club_category.discount
    
    def _get_overrun(self, cr, uid, ccrel, context=None):
        
        if ccrel is None:
            return 0
        
        if not self.browse(cr, uid, ccrel).id_club.country_id:
            return {'warning': {
                'title': _('Warning!'),
                'message' : _('This shopping club has no country assigned.\nPlease assign one.')
            }}
        
        if self.browse(cr, uid, ccrel).id_club.ex_work:
            return 0
        else:
            """
            TODO get value from configuration object
            """
            return 15
        
    def _add_sale_lines(self, cr, uid, ccrel, context=None):

        _c_id = self._get_campaign_id(cr, uid, ccrel, context)
        _club_id = self._get_club_id(cr, uid, [ccrel], context)
        if not _c_id:
            return False
        _product_ids = self._get_products_list(cr, uid, _c_id, context)      
        _saleline_obj = self.pool.get('sales.line')
        _product_obj = self.pool.get('product.product')
        _sp_obj = self.pool.get('product.special.price')
        _lines = []
        
        _supplier_discount_rate = self._get_sp_discount(cr, uid, ccrel, context=None)
        _supplier_overrun_rate = self._get_overrun(cr, uid, ccrel, context)
        for _p_id in _product_ids:
            _sp_id = _sp_obj.search(cr, uid, [('product', '=', _p_id), ('club', '=', _club_id)])
            if _sp_id:
                _cp = _sp_obj.browse(cr, uid, _sp_id).price
            else:
                _cp = _product_obj.browse(cr, uid, _p_id).campaign_price
                _logger.info(_cp)
                _logger.info(_supplier_discount_rate)
                _logger.info(_supplier_overrun_rate)
                _cp = _cp - (_cp * float('0.{0}'.format(_supplier_discount_rate))) if float(_supplier_discount_rate) != 0.0 else _cp
                _cp = _cp + (_cp * float('0.{0}'.format(_supplier_overrun_rate))) if float(_supplier_overrun_rate) != 0.0 else _cp
            _lines.append(
                  _saleline_obj.create(cr, uid, {'product': _p_id,
                                                 'qty': 0,
                                                 'ccrel_id': ccrel,
                                                 'campaign_price': _cp})
            )
            
        self.write(cr, uid, ccrel, {'sales_lines': [(6, 0, _lines)]}, context)
        
        return True
    
    def _check_selling_campaign(self, cr, uid, ids, context=None):

        return True
        """
        self_obj = self.browse(cr, uid, ids[0], context=context)
        id_campaign = self_obj.id_campaign.id
        id_club = self_obj.id_club.id
        date_ini = self_obj.date_ini
        date_end = self_obj.date_end
        
        query = [('id_campaign', '=', id_campaign),
                 ('id_club', '=' , id_club),
                 ('date_ini', '=' , date_ini),
                 ('date_end', '=', date_end)]
        
        search_ids = self.search(cr, uid, query, context=context)
        return not len(search_ids) > 1
        """
        
    def _is_sales_uploaded(self, cr, uid, ids, context=None):
        
        for _id in ids:
            if not self.browse(cr, uid, _id).is_sales_uploades:
                return False
            
        return True
    
    def action_reload_products(self, cr, uid, ids, context=None):
        this = self.browse(cr, uid, ids)
        if this.sales_lines:
            self.write(cr, uid, ids, {'sales_lines': [(2, _sl_line.id) for _sl_line in this.sales_lines]})

        self._add_sale_lines(cr, uid, ids[0], context)
        
    def action_download_csv(self, cr, uid, ids, context=None):
        
        this = self.browse(cr, uid, ids)
        def _order_dict(values={}):
            _ordered_values = collections.OrderedDict()
            if values:
                for key in self._to_read:
                    _ordered_values[key] = values.get(key)
                
                _ordered_values['stock'] = ''
                _ordered_values['sold'] = ''
                return _ordered_values
        
            return None
        
        id_campaign = self._get_campaign_id(cr, uid, ids, context)
        id_club = self._get_club_id(cr, uid, ids, context)
        # id_country = self._get_country_id(cr, uid, ids, context)
        
        products = self._get_products(cr, uid, ids, context)
        p_object = self.pool.get('product.product')
        irattachment = self.pool.get('ir.attachment')
        _sup_name = self.pool.get('res.partner').read(cr, uid, id_club, ['name'])['name']
        if id_campaign:
            _camp_name = self.pool.get('campaign').read(cr, uid, id_campaign, ['name'])['name']
            file_name = '/var/tmp/{0}_{1}_{2}_{3}.csv'.format(id_campaign,
                                                          _camp_name.encode('utf-8'),
                                                          _sup_name.encode('utf-8'),
                                                          time.strftime('%Y-%m-%d_%H-%M'))
        else:
            file_name = '/var/tmp/{0}_{1}_{2}.csv'.format(this.name.encode('utf-8'),
                                                          _sup_name.encode('utf-8'),
                                                          time.strftime('%Y-%m-%d_%H-%M'))
        
        _sl_obj = self.pool.get('sales.line')
        with open(file_name, 'wb') as f:  # Just use 'w' mode in 3.x
            product_header = _order_dict(p_object.read(cr, uid, products[0], self._to_read))
            w = csv.DictWriter(f, product_header.keys(), delimiter=';')
            w.writeheader()
            product_to = []
            for product_id in products:
                _p_sl = _sl_obj.search(cr, uid, [('product', '=', product_id), ('ccrel_id', '=', ids[0])])
                if _p_sl and _p_sl is not False:
                    product_to.append(_order_dict(p_object.read(cr, uid, product_id, self._to_read)))
                    #_p_sl = _sl_obj.read(cr, uid, _p_sl, ['campaign_price'])[0]['campaign_price']
                    product_to[-1]['campaign_price'] = ('{0:.2f}'.format(_sl_obj.browse(cr, uid, _p_sl).campaign_price)).replace('.', ',')
                    product_to[-1]['lst_price'] = ('{0:.2f}'.format(product_to[-1]['lst_price'])).replace('.', ',')
                    try:
                        product_to[-1]['description'] = product_to[-1]['description'].encode('utf-8')
                    except:
                        product_to[-1]['description'] = u'No info'
                    try:
                        product_to[-1]['description_en'] = product_to[-1]['description_en'].encode('utf-8')
                    except:
                        product_to[-1]['description_en'] = u'No info'
                    try:
                        product_to[-1]['name'] = product_to[-1]['name'].encode('utf-8')
                    except:
                        product_to[-1]['name'] = u'No info'
                    
                    product_to[-1]['sold'] = _sl_obj.browse(cr, uid, _p_sl).qty
            w.writerows(product_to)

        with open(file_name, 'rb') as fff:
            attachment_csv = fff.read().encode('base64')
            #stream_csv = base64.b64encode(fff.read())
        
        try: 
            os.remove(file_name) 
        except OSError:
            pass
        
        ir_id = irattachment.search(cr, uid, [('res_model', '=', 'campaigns.clubs.rel'), ('res_id', '=', ids[0]), ('name', 'like', '%.csv')])
        if ir_id:
            irattachment.write(cr, uid, ir_id, {'db_datas': attachment_csv})
        else:
            ir_id = self.pool.get('ir.attachment').create(cr, uid, 
                                                  {'name': file_name.replace('/var/tmp/', ''),
                                                   'datas_fname': file_name.replace('/var/tmp/', ''),
                                                   'db_datas': attachment_csv,
                                                   'type': 'binary',
                                                   'res_model': 'campaigns.clubs.rel',
                                                   'res_id': ids[0]})
        
        return {
                'name'     : 'Download',
                'res_model': 'ir.actions.act_url',
                'type'     : 'ir.actions.act_url',
                'target'   : 'current',
                'url'      : 'web/binary/saveas?model=ir.attachment&field=datas&filename_field=name&id={0}'.format(ir_id[0] if isinstance(ir_id, list) else ir_id)
               }
    
    def action_download_imgs(self, cr, uid, ids, context=None):
        _p_obj = self.pool.get('product.product')
        _p_ids = [line.product.id for line in self.browse(cr, uid, ids).sales_lines]
        if not os.path.exists('/var/tmp/tozip'):
            os.makedirs('/var/tmp/tozip')
        for _p_id in _p_ids:
            _p_info = _p_obj.read(cr, uid, _p_id, ['image', 'product_code'])
            _img = _p_info['image']
            _code = _p_info['product_code']
            if _img:
                with open('/var/tmp/tozip/{0}.jpg'.format(_code), 'w+') as img:
                    img.write(_img.decode('base64'))
        
            _product_images = _p_obj.browse(cr, uid, _p_id).product_images
            if _product_images:
                for _p_image in _product_images:
                    with open('/var/tmp/tozip/{0}'.format(_p_image.filename), 'w+') as img:
                        img.write(_p_image.image.decode('base64'))
                    
        _campaign_name = self.pool.get('campaign').read(cr, uid, self._get_campaign_id(cr, uid, ids, context), ['name'])['name']
        zippath = zipdir('{0}'.format(_campaign_name.encode('utf-8')), '/var/tmp/tozip')
        with open(zippath, 'rb') as fff:
            attachment_csv = fff.read().encode('base64')
            #stream_csv = base64.b64encode(fff.read())
        
        try: 
            os.remove(zippath)
            shutil.rmtree('/var/tmp/tozip')
        except OSError:
            pass
        
        irattachment = self.pool.get('ir.attachment')
        ir_id = irattachment.search(cr, uid, [('res_model', '=', 'campaigns.clubs.rel'), ('res_id', '=', ids[0]), ('name', 'like', '%.zip')])
        if ir_id:
            irattachment.write(cr, uid, ir_id, {'db_datas': attachment_csv})
        else:
            ir_id = self.pool.get('ir.attachment').create(cr, uid, 
                                                  {'name': zippath.replace('/var/tmp/', ''),
                                                   'datas_fname': zippath.replace('/var/tmp/', ''),
                                                   'db_datas': attachment_csv,
                                                   'type': 'binary',
                                                   'res_model': 'campaigns.clubs.rel',
                                                   'res_id': ids[0]})  
        return {
                'name'     : 'Download',
                'res_model': 'ir.actions.act_url',
                'type'     : 'ir.actions.act_url',
                'target'   : 'current',
                'url'      : 'web/binary/saveas?model=ir.attachment&field=datas&filename_field=name&id={0}'.format(ir_id[0] if isinstance(ir_id, list) else ir_id)
               }
        
    def action_generate_invoice(self, cr, uid, ids, context=None):
        _logger.info('Generating invoice')
        club_id = self._get_club_id(cr, uid, ids, context)
        ccrel_id = ids[0]
        invoice_id = self._get_invoice(cr, uid, ids, context)
        invoiced_products = []
        
        ref = False
        
        product_obj = self.pool.get('product.product')
        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')
        saleline_obj = self.pool.get('sales.line')
        
        if not invoice_id:
            invoice_id = invoice_obj.create(cr, uid,
                                            {'ref': self.pool.get('ir.sequence').get(cr, uid, 'invoice.ref'),
                                             'partner_id': club_id,
                                             'account_id': self._clients_account_id})
            self.write(cr, uid, ids, {'invoice': invoice_id})
        else:
            _logger.info('already invoiced')
            invoiced_products, invoice_line_ids = self._get_invoiced_products(cr, uid, ids, context=None)
            
        sales_lines = self.read(cr, uid, ids[0], ['sales_lines'])['sales_lines']
        for sl_id in sales_lines:
            sl_info = saleline_obj.read(cr, uid, sl_id, ['product', 'qty', 'campaign_price'])
            sl_prod_id, sl_qty, sl_price = sl_info['product'][0], sl_info['qty'], sl_info['campaign_price']
            country_id = self.pool.get('res.partner').read(cr, uid, club_id, ['country_id'])['country_id'][0]
            if not country_id:
                raise osv.except_osv(
                    _('Error!'),
                    _('The shopping club must have a valid country assigned.'))

            if sl_qty > 0:
                if sl_prod_id not in invoiced_products:
                    invoiced_products.append(invoice_line_obj.create(cr, uid, {'product_id': sl_prod_id,
                                                                              'name': product_obj.read(cr, uid, sl_prod_id, ['name'])['name'],
                                                                              'invoice_id': invoice_id,
                                                                              'price_unit': sl_price,
                                                                              'quantity': sl_qty,
                                                                              'invoice_line_tax_id': [(6, 0, [self._vat_id])] if int(country_id) == self._spain_id else False
                                                                              }))
                else:
                    _logger.info('updating line')
                    invoice_line_obj.write(cr, uid, invoice_line_ids[invoiced_products.index(sl_prod_id)], {'price_unit': sl_price, 'quantity': sl_qty})
   
        return True
    
    
    def action_generate_sale_order(self, cr, uid, ids, context=None):
        _logger.info('Generating sale order')
        club_id = self._get_club_id(cr, uid, ids, context)
        saleorder_id = self._get_sale_order(cr, uid, ids, context)
        saleorder_products = []
        saleorder_line_ids = []
        
        product_obj = self.pool.get('product.product')
        saleorder_obj = self.pool.get('sale.order')
        saleorder_line_obj = self.pool.get('sale.order.line')
        saleline_obj = self.pool.get('sales.line')
        
        if not saleorder_id:
            saleorder_id = saleorder_obj.create(cr, uid,
                                            {# 'ref': self.pool.get('ir.sequence').get(cr, uid, 'invoice.ref'),
                                             'partner_id': club_id,
                                             # 'account_id': self._clients_account_id,
                                             })
            self.write(cr, uid, ids, {'sale_order': saleorder_id})
        else:
            saleorder_products, saleorder_line_ids = self._get_ordered_products(cr, uid, ids, context)
            
        sales_lines = self.read(cr, uid, ids[0], ['sales_lines'])['sales_lines']
        _logger.info(sales_lines)
        try:
            country_id = self.pool.get('res.partner').read(cr, uid, club_id, ['country_id'])['country_id'][0]
        except:
            raise osv.except_osv(
                _('Error!'),
                _('The shopping club must have a country assigned on the billing addres tab.'))
  
        for sl_id in sales_lines:
            _logger.info(sl_id)
            sl_info = saleline_obj.read(cr, uid, sl_id, ['product', 'qty', 'campaign_price'])
            sl_prod_id, sl_qty, sl_price = sl_info['product'][0], sl_info['qty'], sl_info['campaign_price']

            if sl_qty > 0:
                if sl_prod_id not in saleorder_products:
                    saleorder_products.append(saleorder_line_obj.create(cr, uid, {'product_id': sl_prod_id,
                                                                              'name': product_obj.read(cr, uid, sl_prod_id, ['name'])['name'],
                                                                              'order_id': saleorder_id,
                                                                              'price_unit': sl_price,
                                                                              'product_uom_qty': sl_qty,
                                                                              'tax_id': [(6, 0, [self._vat_id])] if int(country_id) == self._spain_id else False
                                                                              }))
                else:
                    _logger.info(sl_prod_id)
                    saleorder_line_obj.write(cr, uid, saleorder_line_ids[saleorder_products.index(sl_prod_id)], {'price_unit': sl_price, 'product_uom_qty': sl_qty})

        sale_order_tree = self.pool.get('ir.model.data')._get_id(cr, uid, 'sale', 'view_order_tree')
        return { 
            'name': 'Quotations',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'sale.order',
            'type': 'ir.actions.act_window',
            'search_view_id': sale_order_tree,
        }
        
    def action_send_to_production(self, cr, uid, ids, context=None):
        
        def _compose_body(order):
            
            _body = _("""
                    Hi, {0}\n
                    the order {1} is ready to process, you can check it in your supplier board.
                    """.format(order.supplier_id.name.encode('utf-8'), order.reference))
            return _body
        
        this = self.browse(cr, uid, ids)
        if not this.supplier_order_details:
            raise osv.except_osv(_('Error!'), _('No supplier orders found.'))
        
        for order in this.supplier_order_details:
            order.write({'status':'pending'})
            msg = {
                'type': 'notification',
                'author_id': self.pool.get('res.users').browse(cr, uid, uid).partner_id.id, #uid,
                'partner_ids': [(4, order.supplier_id.id)],
                'model': 'supplier.order.details',
                'res_id': order.id,
                'body': _compose_body(order),
                'subject': 'New order ready to process.',
            }
            self.pool.get('mail.message').create(cr, uid, msg, context)
            
        this.write({'state': 'in_production'})
    
    def action_set_as_pendent(self, cr, uid, ids, context=None):
        this = self.browse(cr, uid, ids)
        this.write({'state': 'pendent'})
        
    def action_set_as_active(self, cr, uid, ids, context=None):
        this = self.browse(cr, uid, ids)
        this.write({'state': 'on'})
    
    def action_set_as_finished(self,cr, uid, ids, context=None):
        this = self.browse(cr, uid, ids)
        if all([sod.status == 'completed' for sod in this.supplier_order_details]): 
            this.write({'state': 'finished'})
        else:
            raise osv.except_osv(_('Warning'), _('There are incomplete supplier orders.'))
        
    def action_show_sales_wizard(self, cr, uid, ids, context=None):
        
        view_ref = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'shopping_clubs', 'view_wizard_upload_sales')
        view_id = view_ref and view_ref[1] or False,
        return {
            'type': 'ir.actions.act_window',
            'name': _('Upload sales'),
            'res_model': 'upload.sales',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'target': 'new',
            'context': {'ccrel_id': ids[0]}
        }
        
    def action_supplier_order_wizard(self, cr, uid, ids, context=None):
        
        view_ref = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'shopping_clubs', 'supplier_order_wizard')
        view_id = view_ref and view_ref[1] or False,
        return {
            'type': 'ir.actions.act_window',
            'name': _('Generate manufacturing orders'),
            'res_model': 'supplier.order',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'target': 'new',
            'context': {'ccrel_id': ids[0]}
        }
    
    def action_missing_units(self, cr, uid, ids, context=None):
    
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        this = self.browse(cr, uid, ids)
        
        line_ids = [line.id for order in this.supplier_order_details for line in order.line_details]
        sodl = self.pool.get('supplier.order.details.line')
        rows = [collections.OrderedDict({'Ean': line.product.ean13,
                                         'Product': line.product.name, 
                                         'Missing': line.qty - line.qty_picked,
                                         'Order': line.sod_id.reference}) for line in sodl.browse(cr, uid, line_ids)]
        
        rows[:] = [row for row in rows if int(row.get('Missing')) > 0]
        
        if rows:
            cso = StringIO()
            csv_writer = csv.DictWriter(cso, rows[0].keys(), delimiter=';')
            csv_writer.writeheader()
            csv_writer.writerows(rows)
            
            _name = this.name.encode('utf-8')
            irattachment = self.pool.get('ir.attachment')
            ir_id = irattachment.search(cr, uid, [('res_model', '=', 'campaigns.clubs.rel'), ('res_id', '=', ids[0]), ('name', 'like', 'Missing_units_{0}.csv'.format(_name))])
            if ir_id:
                irattachment.write(cr, uid, ir_id, {'db_datas': cso.getvalue().encode('base64')})
            else:
                ir_id = irattachment.create(cr, uid, 
                                            {'name': 'Missing_units_{0}.csv'.format(_name),
                                             'datas_fname': 'Missing_units_{0}.csv'.format(_name),
                                             'db_datas': cso.getvalue().encode('base64'),
                                             'type': 'binary',
                                             'res_model': 'campaigns.clubs.rel',
                                             'res_id': ids[0]})
            
            return {
                    'name'     : 'Download',
                    'res_model': 'ir.actions.act_url',
                    'type'     : 'ir.actions.act_url',
                    'target'   : 'current',
                    'url'      : 'web/binary/saveas?model=ir.attachment&field=datas&filename_field=name&id={0}'.format(ir_id[0] if isinstance(ir_id, list) else ir_id)
            }
    
    def action_palletized_units(self, cr, uid, ids, context=None):
    
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        this = self.browse(cr, uid, ids)
        
        line_ids = [line.id for order in this.supplier_order_details for line in order.line_details]
        sodl = self.pool.get('supplier.order.details.line')
                
        rows = [collections.OrderedDict({'Ean': line.product.ean13,
                                         'Product': line.product.name, 
                                         'Palletized': line.qty_picked,
                                         'Order': line.sod_id.reference}) for line in sodl.browse(cr, uid, line_ids)]
        
        if rows:
            cso = StringIO()
            csv_writer = csv.DictWriter(cso, rows[0].keys(), delimiter=';')
            csv_writer.writeheader()
            csv_writer.writerows(convert(rows))
    
            _name = this.name.encode('utf-8')
            irattachment = self.pool.get('ir.attachment')
            ir_id = irattachment.search(cr, uid, [('res_model', '=', 'campaigns.clubs.rel'), ('res_id', '=', ids[0]), ('name', 'like', 'Palletized_units_{0}.csv'.format(_name))])
            if ir_id:
                irattachment.write(cr, uid, ir_id, {'db_datas': cso.getvalue().encode('base64')})
            else:
                ir_id = irattachment.create(cr, uid, 
                                            {'name': 'Palletized_units_{0}.csv'.format(_name),
                                             'datas_fname': 'Palletized_units_{0}.csv'.format(_name),
                                             'db_datas': cso.getvalue().encode('base64'),
                                             'type': 'binary',
                                             'res_model': 'campaigns.clubs.rel',
                                             'res_id': ids[0]})
            
            return {
                    'name'     : 'Download',
                    'res_model': 'ir.actions.act_url',
                    'type'     : 'ir.actions.act_url',
                    'target'   : 'current',
                    'url'      : 'web/binary/saveas?model=ir.attachment&field=datas&filename_field=name&id={0}'.format(ir_id[0] if isinstance(ir_id, list) else ir_id)
            }
    
    def action_dummy(self, cr, uid, ids, context=None):
        _logger.info(uid)
        _logger.info(ids)
        _logger.info(self.pool.get('res.users').browse(cr, uid, uid).partner_id.id)
        
    def has_sale_lines(self, cr, uid, ids, context=None):
        return False
        
    def is_finished(self, cr, uid, ids=None, context=None):
        ids = self.search(cr, uid, [('state', '=', 'on')], context)     
        for ccr_id in ids:
            _date = self.read(cr, uid, ccr_id, ['date_end'])['date_end']
            if time.strptime(_date, self._date_format) < time.strptime(time.strftime(self._date_format), self._date_format):
                self.write(cr, uid, ids, {'state': 'finished'})

        ids = self.search(cr, uid, [('state', '=', 'pendent')], context)
        for ccr_id in ids:
            _date = self.read(cr, uid, ccr_id, ['date_ini'])['date_ini']
            if time.strptime(_date, self._date_format) == time.strptime(time.strftime(self._date_format), self._date_format):
                self.write(cr, uid, ids, {'state': 'on'})
        
        return True
    
    def create(self, cr, uid, vals, context=None):
        record_id = super(campaigns_clubs_rel, self).create(cr, uid, vals, context=context)
        #_date = self.read(cr, uid, record_id, ['date_end'])['date_end']

        self._add_sale_lines(cr, uid, record_id, context)
        """
        if time.strptime(_date, self._date_format) <= time.strptime(time.strftime(self._date_format), self._date_format):
            self.write(cr, uid, record_id, {'state': 'finished'}, context)
        else:
            self.write(cr, uid, record_id, {'state': 'pendent'}, context)
        """ 
        return record_id
    
    def name_get(self, cr, uid, ids, context=None):
        res = []
        for rec in self.browse(cr, uid, ids, context):
            name = rec.name if rec.name else rec.id_campaign.name 
            res.append((rec.id, name))
        return res
    
    _columns = {
        'name': fields.char('Name', size=24),
        'id_campaign': fields.many2one('campaign', 'Campaign', ondelete='restrict'),  # ID CAMPAING, many2one
        'id_club': fields.many2one('res.partner', 'Shopping club', domain="[('is_shopping_club','=',True)]", ondelete='restrict', required=True),  # ID CLUB, many2one
        'id_country': fields.many2many('res.country', 'campaigns_countries_rel', 'campaing_clubs_id', 'country_id', 'Countries'),  # ID COUNTRY, many2one
        'date_ini': fields.date('Starting date', required=True),  # DATE INI
        'date_end': fields.date('Ending date', required=True),  # DATE END
        'state': fields.selection(_get_selection, 'State'),
        'file_name': fields.char('File name', size=50),
        'file_stream': fields.binary('CSV file', readonly=True),
        'sales_lines': fields.one2many('sales.line', 'ccrel_id', 'Lines'),
        'is_sales_uploaded': fields.boolean('Sales uploaded'),
        'invoice': fields.many2one('account.invoice', string='Invoice'),
        'sale_order': fields.many2one('sale.order', string='Sale order'),
        'supplier_order_details': fields.one2many('supplier.order.details', 'ccrel_id', 'Supplier orders'),
        'amount_total': fields.function(_get_order_amount, type="float", string="Budget", digits_compute=dp.get_precision('Product Price')),
        'total_units': fields.function(_get_total_units, type="integer", string="Units sold"),
        'purchase_order': fields.char('Purchase order', size=15),
        'campaign_number': fields.char('Campaing number', size=15),
        'reference': fields.function(_get_reference, string='Reference', store=True, type='char'),
        'delivery_name': fields.char('Delivery name', size=55),
        'delivery_street': fields.char('Street', size=55),
        'delivery_city': fields.char('City', size=55),
        'delivery_zip': fields.char('Post code', size=10),
        'delivery_state': fields.char('State', size=25),
        'delivery_country': fields.char('Country', size=25),
        'delivery_date': fields.date('Delivery date', help="Date when the goods will be collected on supplier's warehouse."),
        'pack_individual': fields.boolean('Individual packing', help="Check this field if goods must be shipped with individual packing.")
    }

    _defaults = {
        'state': 'pendent',
        'is_sales_uploaded': False
    }
    
    _constraints = [
        (_check_selling_campaign, 'A selling campaign with the same data already exists.', ['id_campaign', 'id_club', 'date_ini', 'date_end'])
    ]
    
    def print_quotation(self, cr, uid, ids, context=None):
        '''
        This function prints the sales order and mark it as sent, so that we can see more easily the next step of the workflow
        '''
        assert len(ids) == 1, 'This option should only be used for a single id at a time'
        return self.pool['report'].get_action(cr, uid, ids, 'shopping_clubs.report_ccrel_details', context=context)

    def action_getfile(self, cr, uid, ids, context=None):
        _logger.info('Hey')
  
campaigns_clubs_rel()

class sales_line(orm.Model):
    
    _name = 'sales.line'
    
    def _get_default_code(self, cr, uid, ids, field, arg, context=None):
        try:
            return dict([(x, self.browse(cr, uid, x).product.default_code) for x in ids])
        except Exception as ex:
            _logger.info(ex)
            return False
        
        return False
    
    def _get_product_code(self, cr, uid, ids, field, arg, context=None):
        try:
            return dict([(x, self.browse(cr, uid, x).product.product_code) for x in ids])
        except Exception as ex:
            _logger.info(ex)
            return False
        
        return False
    
    def _get_product_image(self, cr, uid, ids, field, arg, context=None):
        try:
            return dict([(x, self.browse(cr, uid, x).product.image_small) for x in ids])
        except Exception as ex:
            _logger.info(ex)
            return False
        
        return False
    
    _columns = {
        'product': fields.many2one('product.product', 'Name', 'Products'),
        'default_code': fields.function(_get_default_code, type="char", string="Internal reference"),
        'product_code': fields.function(_get_product_code, type="char", string="Product code"),
        'special_info': fields.char('Info', size=30),
        'product_image': fields.function(_get_product_image, type="binary", string="Image"),
        'qty': fields.integer('Quantity'),
        'ccrel_id': fields.many2one('campaigns.clubs.rel', 'campagins.clubs.rel_id', 'Shelling campaign', ondelete="cascade"),
        'campaign_price': fields.float('Campaign Price', digits_compute=dp.get_precision('Product Price'), help="Price offered to shopping clubs."),
    }
    
    def onchange_product(self, product):
        if product:
            product_obj = self.env['product.product'].browse(product.id)
            return {'value': {'campaing_price': product_obj.campaign_price}}
        
        return {}
        
    def write(self, cr, uid, ids, vals, context=None):
        if vals and 'qty' in vals.keys():            
            if not self.browse(cr, uid, ids).ccrel_id.is_sales_uploaded:
                self.browse(cr, uid, ids).ccrel_id.write({'is_sales_uploaded': True})
            
            sod_obj = self.pool.get('supplier.order.details')
            sodl_obj = self.pool.get('supplier.order.details.line')
            
            ccrel_id = self.browse(cr, uid, ids).ccrel_id.id
            p_id = self.browse(cr, uid, ids).product.id
            sod_ids = sod_obj.search(cr, uid, [('ccrel_id', '=', ccrel_id)])
            sodl_ids = sodl_obj.search(cr, uid, [('sod_id', 'in', sod_ids)])
            for sodl in sodl_obj.browse(cr, uid, sodl_ids):
                if p_id and sodl.product.id == p_id:
                    sodl.write({'qty': vals.get('qty')})
        return super(sales_line, self).write(cr, uid, ids, vals, context=context)

sales_line()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
