# -*- coding: utf-8 -*-
########################
# Created on 10/9/2014
#
# @author: Oscar Blanco
########################

positions = {
    'name': 2,
    'supplier_ref': 1,
    'stock': 3,
    'pvd': 4,
    'pvp': 5,
    'desc': 6,
    'diameter': 9,
    'height': 10,
    'length': 11,
    'width': 12,
    'package': 13,
    'weight': 14,
    'package_weight': 15,
    'p_height': 16,
    'p_length': 17,
    'p_width': 18,
    'sales_qty': -1
}

sales_positions = {
    'ref': 0,
    'price': 1,
    'sales_qty': 2
}