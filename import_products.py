# -*- coding: utf-8 -*-
########################
# Created on 28/8/2014
#
# @author: openerp
########################

import openerp
import logging

from openerp import netsvc, tools, pooler
from openerp.osv import fields, orm, osv
from openerp.tools.translate import _

import csv
import base64
from StringIO import StringIO
from string import replace

import glob

from translator import Translator

_logger = logging.getLogger(__name__)
_pcode_placeholder = '{0:02d}{1:05d}'

class import_products(osv.osv_memory):
    '''
    Import products wizard
    '''
    
    _positions = {
        'supplier_ref': 1,
        'name': 2,
        'pc': 3,
        'pvp': 4,
        'pvd': 5,
        'desc': 6,
        'diameter': 9,
        'height': 10,
        'length': 11,
        'width': 12,
        'package': 13,
        'weight': 14,
        'package_weight': 15,
        'p_height': 16,
        'p_length': 17,
        'p_width': 18,
        'material': 19,
        'material_qty': 20,
        'material_unit': 21,
        'tags': 22,
        'manufacturing': 23,
        'product_class': 24,
    }
    
    _pcode_placeholder = _pcode_placeholder
    _translator = None
    
    _name = 'import.products'
    _rec_name = 'csv_file'
    
    _columns = {
        'csv_file': fields.binary('CSV file'),
        # 'supplier': fields.selection(_get_suppliers, 'Supplier'),
        'supplier': fields.many2one('res.partner', 'Supplier', required=True),
        'category': fields.many2one('product.category', 'Category', required=True),
        'class': fields.many2one('product.class', 'Product class', required=True),
        'state': fields.selection([('init', 'init'), ('done', 'done')], 'state', readonly=True),
        'to_buy': fields.boolean('Products are bought?'),
        'img_path': fields.char('Path', size=48),
    }
    
    _defaults = {
        'state': 'init'
    }
    
    def __init__(self, pool, cr):
        super(osv.osv_memory, self).__init__(pool, cr)
        self._translator = Translator()
        
    def _check_categories_id(self, cr, uid, ids=None, context=None):
        cr.execute("select exists(select 1 from product_category_id_seq where last_value > 9)")
        if cr.fetchone()[0]:
            return
        else:
            cr.execute("ALTER SEQUENCE product_category_id_seq RESTART WITH 10")
        
    def _insert_product(self, cr, uid):
        raise NotImplementedError('This method isn\'t implemented')
    
    def _get_image(self, cr, uid, product_ref, img_path):
        return glob.glob('{0}{1}*.jpg'.format(img_path, product_ref))
    
    def _get_base64img(self, imgs):
        try:
            if imgs:
                with open(imgs[0], "rb") as image_file:
                    return base64.b64encode(image_file.read())
            else:
                return False
        except:
            return False
    
    def _get_product_class_id(self, cr, uid, fields, context=None):
        try:
            product_class = fields[self._positions['product_class']]
            if product_class.strip():
                _pc_id = self.pool.get('product.class').search(cr, uid, [('name', '=', self.encode_string(product_class.strip()))])
                if not _pc_id:
                    raise osv.except_osv(
                        _('Error!'),
                        _('Product class {0} doesn\'t exists.'.format(product_class)))    
                return _pc_id[0]
            else:
                return False
        except Exception as ex:
            _logger.error('No product class found on file.')
            return False
        
    def _check_number(self, num):
        if ',' in num:
            num = num.replace(',', '.')
            
        return num
    
    def encode_string(self, string):
        try:
            return string.decode('utf-8')
        except:
            return string.encode('utf-8')
            
        return False
    
    def get_product_tags(self, cr, uid, ids, fields, context=None):
        
        if not fields or len(fields) < 1:
            return False
        
        _tag_obj = self.pool.get('product.tag')
        _tags = fields[self._positions['tags']].split(',')
        _ids = []
        for _tag in _tags:
            _tag_id = _tag_obj.search(cr, uid, [('name', '=', self.encode_string(_tag).capitalize())])
            if _tag_id:
                _ids.extend(_tag_id)
            else:
                _ids.append(_tag_obj.create(cr, uid, {'name': self.encode_string(_tag).capitalize()}))
        
        
        return list(set(_ids))
    
    
    def write_material_lines(self, cr, uid, ids, product_id, fields, context=None):
        
        if not fields[self._positions['material']].strip() \
            or not not fields[self._positions['material_qty']].strip() \
            or not not fields[self._positions['material_unit']].strip():
            return False
        
        _pm_obj = self.pool.get('product.material')
        _pmll_obj = self.pool.get('product.material.list.line')
        _pu_obj = self.pool.get('product.uom')
        
        _p_mat = fields[self._positions['material']].split(',')
        _p_mat_qty = fields[self._positions['material_qty']].split(',')
        _p_mat_unit = fields[self._positions['material_unit']].split(',')
        
        _ids = []
        for n in range(len(_p_mat)):
            _p_mat_id = _pm_obj.search(cr, uid, [('name', '=', self.encode_string(_p_mat[n]))])
            if not _p_mat_id:
                raise osv.except_osv(
                    _('Error!'),
                    _('Material {0} doesn\'t exists.'.format(self.encode_string(_p_mat[n]))))
                
                # # This lines create the product material if does not exists, just uncomment 
                """
                _p_mat_id =_pm_obj.create(cr, uid, {'name': self.encode_string(_p_mat[n]).capitalize()})
                _logger.info('p-mat-id c -> {0}'.format(_p_mat_id))
                """
            _p_mat_unit_id = _pu_obj.search(cr, uid, [('name', 'ilike', self.encode_string(_p_mat_unit[n]).capitalize())])
            if not _p_mat_unit_id:
                raise osv.except_osv(
                    _('Error!'),
                    _('Invalid unit of measure: {0}'.format(self.encode_string(_p_mat_unit[n]).capitalize())))
                
                # # This lines create the product uom if does not exists, just uncomment
                # # Needs a little tweaking, :( 
                """
                _p_mat_unit_id =_pu_obj.create(cr, uid, {'name': self.encode_string(_p_mat_unit[n]).capitalize()})
                _logger.info('p-mat-unit-id c -> {0}'.format(_p_mat_unit_id))
                """
            """
            self.pool.get('product.product').write(cr, uid, product_id, {'product_id': product_id,
                                                                         'qty': _p_mat_qty[n],
                                                                         'product_material_id': _p_mat_id,
                                                                         'uom_id': _p_mat_unit_id })
            """

            _ids.append(_pmll_obj.create(cr, uid, {'product_id': product_id[0] if isinstance(product_id, list) else product_id,
                                                   'qty': _p_mat_qty[n],
                                                   'product_material_id': _p_mat_id[0] if isinstance(_p_mat_id, list) else _p_mat_id,
                                                   'uom_id': _p_mat_unit_id[0] if isinstance(_p_mat_unit_id, list) else _p_mat_unit_id
                                                   }))
            
        return _ids

    def get_manufacturing_ids(self, cr, uid, ids, fields, context=None):
    
        _ids = []
        try:
            if not fields[self._positions['manufacturing']].strip():
                return False
        except:
            return False
        
        _manufacturing = fields[self._positions['manufacturing']].split(',')
        _manu_obj = self.pool.get('manufacturing.type')
        for _m in _manufacturing:
            _m = _m.strip()
            _m_id = _manu_obj.search(cr, uid, [('name', '=', self.encode_string(_m))])

            if _m_id:
                _ids.extend(_m_id)
            else:
                raise osv.except_osv(
                    _('Error!'),
                    _('Manufacturing type {0} doesn\'t exists.'.format(_m)))
            
        return _ids
    
    def _update_product(self, cr, uid, fields, product_obj, product_ids, imgs, class_id, translator):
        product_obj.write(
                cr, uid, product_ids,
                {'list_price': self._check_number(fields[self._positions['pvp']]),
                 'campaign_price': self._check_number(fields[self._positions['pvd']]),
                 'standard_price': self._check_number(fields[self._positions['pc']]),
                 # 'image_medium': self._get_base64img(imgs),
                 # 'product_code': self._pcode_placeholder.format(product_obj.browse(cr, uid, uid, None).categ_id, int(fields[self._positions['supplier_ref']])),
                 'description_en': self._translator.translate(fields[self._positions['desc']]),
                 'description': self.encode_string(fields[self._positions['desc']]),
                 'product_class': class_id,
                 'height': fields[self._positions['height']],
                 'width': fields[self._positions['width']],
                 'length': fields[self._positions['length']],
                 'packing_height': fields[self._positions['p_height']],
                 'packing_width': fields[self._positions['p_width']],
                 'packing_length': fields[self._positions['p_length']],
                 'volume': (float(fields[self._positions['p_height']]) * float(fields[self._positions['p_width']]) * float(fields[self._positions['p_length']])) / 1000000
                })
        
        _p_info = product_obj.read(cr, uid, product_ids, ['product_tags', 'product_material_lines', 'manufacturing'])[0]
        _values = {}
        if not _p_info.get('product_tags'):
            _tag_ids = self.get_product_tags(cr, uid, [], fields, None)
            _values['product_tags'] = [(6, 0, _tag_ids)]
        
        if not _p_info.get('product_material_lines'):
            _values['product_material_lines'] = [(6, 0, self.write_material_lines(cr, uid, [], product_ids, fields, None))]
            
        if not _p_info.get('manufacturing'):
            _manufacturing_ids = self.get_manufacturing_ids(cr, uid, [], fields, None)
            if _manufacturing_ids:
                _values['manufacturing'] = [(6, 0, _manufacturing_ids)]

        if len(_values) > 0:
            product_obj.write(cr, uid, product_ids, _values)
        
        _logger.info('Updating')
        return product_ids[-1]

    def import_products(self, cr, uid, ids, context=None):
        
        # prods = []
        product_obj = self.pool.get('product.product')
        # partner_obj = self.pool.get('res.partner')
        # categ_obj = self.pool.get('product.category')
        product_supplier = self.pool.get('product.supplierinfo')
        res = self.read(cr, uid, ids, ['csv_file', 'supplier', 'category', 'class', 'to_buy'])
        
        img_path = self.pool.get('custom.config.settings').get_default_img_path(cr, uid, {})
        img_path = img_path.get('path')
        
        res = res and res[0] or {}
        _logger.info('RES is -> {0}'.format(res))
        supplier_id = res['supplier'][0]
        category_id = res['category'][0]
        class_id = res['class'][0]
        to_buy = res['to_buy']
        
        _route_name = 'Buy' if to_buy else 'Make To Order'
        _route_ids = self.pool.get('stock.location.route').search(cr, uid, [('name', '=', _route_name)])
        _logger.info('To buy -> {0}'.format(to_buy))
        # if (supplier_id is None or not supplier_id):
        prod_id = None
        for row in StringIO(base64.decodestring(res['csv_file'])):
            fields = row.split('|')
            products = product_obj.search(cr, uid, [('default_code', '=', fields[self._positions['supplier_ref']])])
            # Get images
            imgs = self._get_image(cr, uid, fields[self._positions['supplier_ref']], img_path)
            _pc_id = self._get_product_class_id(cr, uid, fields, context)
            # Check if this product already exists on db.
            if products:
                # Product exists, update
                prod_id = self._update_product(cr, uid, fields, product_obj, products, imgs, class_id if not _pc_id else _pc_id, self._translator)
            else:
                # No product found, insert
                encoded_img = self._get_base64img(imgs)
                _manufacturing_ids = self.get_manufacturing_ids(cr, uid, ids, fields, context)
                prod_id = product_obj.create(cr, uid,
                                          {'type': 'product',
                                           'default_code': fields[self._positions['supplier_ref']],
                                           'categ_id': category_id,
                                           'product_class': class_id if not _pc_id else _pc_id,
                                           'list_price': self._check_number(fields[self._positions['pvp']]),
                                           'campaign_price': self._check_number(fields[self._positions['pvd']]),
                                           'standard_price': self._check_number(fields[self._positions['pc']]),
                                           'name': self.encode_string(fields[self._positions['name']]),
                                           'description': self.encode_string(fields[self._positions['desc']]),
                                           'description_en': self._translator.translate(fields[self._positions['desc']]),
                                           'weight': self._check_number(fields[self._positions['package_weight']]),
                                           'weight_net': self._check_number(fields[self._positions['weight']]),
                                           'image': encoded_img,
                                           'height': fields[self._positions['height']],
                                           'width': fields[self._positions['width']],
                                           'length': fields[self._positions['length']],
                                           'packing_height': fields[self._positions['p_height']],
                                           'packing_width': fields[self._positions['p_width']],
                                           'packing_length': fields[self._positions['p_length']],
                                           'manufacturing': [(6, 0, _manufacturing_ids)] if _manufacturing_ids else False,
                                           'product_tags': [(6, 0, self.get_product_tags(cr, uid, ids, fields, context))],
                                           'route_ids': [(6, 0, _route_ids)]
                                           })
                # Write custom product code
                #product_obj.write(cr, uid, prod_id, {'product_code': self._pcode_placeholder.format(category_id, prod_id)})
                # Write material lines
                product_obj.write(cr, uid, prod_id, {'product_material_lines': [(6, 0, self.write_material_lines(cr, uid, ids, prod_id, fields, context))]})
                # Get product template id and relate it to a supplier
                prod_tmpl_id = product_obj.read(cr, uid, prod_id, ['product_tmpl_id'])
                prod_tmpl_id = [value for key, value in prod_tmpl_id.items() if key == 'product_tmpl_id'][0][0]
                product_supplier.create(cr, uid, {'name':supplier_id, 'product_tmpl_id':prod_tmpl_id, 'product_code':fields[0], 'min_qty':1})
        
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'product.product',
            'view_mode': 'form',
            'res_id': prod_id if prod_id is not None else False
        }
        
import_products()

class custom_config_settings(orm.Model):

    _name = 'custom.config.settings'
    _inherit = 'res.config.settings'
    _columns = {
        'path': fields.char('Path', size=48, required=True),
        'manufacturer_code': fields.char('Manufacturer code', size=7, required=True)
    }
 
    def get_default_img_path(self, cr, uid, fields, context=None):
        try:
            setting_id = self.search(cr, uid, [])[0]
            return {'path': self.browse(cr, uid, setting_id).path}
        except:
            return {'path':'/var/tmp/img/'}

    def set_img_path(self, cr, uid, ids, context=None):
        config = self.search(cr, uid, [])[0]
        new_path = self.browse(cr, uid, config, context).path
        self.write(cr, uid, config, {'path': new_path})
  
    def get_default_manufacturer_code(self, cr, uid, fields, context=None):
        try:
            setting_id = self.search(cr, uid, [])[0]
            return {'manufacturer_code': self.browse(cr, uid, setting_id).manufacturer_code}
        except:
            return False

    def set_manufacturer_code(self, cr, uid, ids, context=None):
        config = self.search(cr, uid, [])[0]
        new_code= self.browse(cr, uid, config, context).manufacturer_code
        self.write(cr, uid, config, {'manufacturer_code': new_code})
    
    def create(self, cr, uid, vals, context=None):
        _cur_ids = self.search(cr, uid, [])
        if _cur_ids:
            record_id = _cur_ids[0]
            self.write(cr, uid, record_id, {'path': vals['path'], 'manufacturer_code': vals['manufacturer_code']})
        else:
            record_id = super(custom_config_settings, self).create(cr, uid, vals, context=context)
            
        return record_id
    
custom_config_settings()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
