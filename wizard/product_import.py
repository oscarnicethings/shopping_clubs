# -*- coding: utf-8 -*-
########################
# Created on Apr 9, 2015
#
# @author: openerp
########################

import logging

from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning

from .. translator import Translator
from cStringIO import StringIO
import csv

_logger = logging.getLogger(__name__)

class product_import(models.TransientModel):
    '''
    classdocs
    '''
    
    _name = 'product.import'
    
    csv_file = fields.Binary(string = 'CSV file', required = True)
    supplier = fields.Many2one('res.partner', help="All products are related to this supplier.")
    category = fields.Many2one('product.category', help="All products are belong to this category.")
    product_class = fields.Many2one('product.class', help="All products are belong to this product class.")
    to_buy = fields.Boolean('Products are bought?')
    step = fields.Selection((('load', 'Load file'), ('match', 'Match fields')))
    delimiter = fields.Selection(((',', ','), (';', ';'), ('|', '|')), required = True)
    
    _defaults = {'step': 'load', 'delimiter': ';'}
    
    def __init__(self, cr, pool):
        super(models.TransientModel, self).__init__(cr, pool)
        self._translator = Translator()
    
    @api.multi
    def encode(self, value):
        _logger.info('value is -> {0}'.format(value))
        if '__export__' in value:
            value = value.split('_')[-1]
            
        _logger.info('and now value is -> {0}'.format(value))
        try:
            float(value)
            if ',' in value or '.' in value:
                return float(value)
            else:
                return int(value)
        except:
            pass
        
        try:
            return value.decode('uft-8')
        except:
            return value

    @api.one
    def action_load(self):
        product_obj = self.env['product.product']
        
        fields_got = product_obj.fields_get()
        _logger.info(fields_got.keys())
        _logger.info(fields_got.get('seller_ids'))
        #map(_logger.info, [(key, value) for dd in fields_got for key, value in dd if key != 'help'])
        
        lines = csv.DictReader(StringIO(self.csv_file).read().decode('base64').splitlines(), delimiter = '{0}'.format(self.delimiter.encode('utf-8')))
        for row in lines:
            #row = {key.split('/')[0] if '/' in key else key: self.encode(value) for key, value in row.iteritems()}
           
            _logger.info(row)
            _p_id = row.get('id')
            product_obj.load(row.keys(), row)
            """
            if _p_id:
                product = product_obj.search([('id', '=', _p_id)])
                if product:
                    _logger.info('YES')
                    # UPDATE
                else:
                    # NOT FOUND, show warning
                    _logger.info('NO')
                _logger.info(product.id)
                _logger.info(product.seller_id)
            ## CREATE PRODUCT
            else:
                product_obj.create(row)
            """
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: