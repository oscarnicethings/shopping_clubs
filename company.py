# -*- coding: utf-8 -*-
########################
# Created on 30/9/2014
#
# @author: openerp
########################

import openerp
import logging

from openerp.osv import fields, orm
from openerp.tools.translate import _

_logger = logging.getLogger(__name__)

class company(orm.Model):
    '''
    classdocs
    '''
    
    _inherit = 'res.company'
    
    _columns = {
        'cif': fields.char('CIF', size=9)
    }

                         
company()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
