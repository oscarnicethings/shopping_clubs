# -*- coding: utf-8 -*-
########################
# Created on 18/12/2014
#
# @author: openerp
########################

import openerp
import logging

from openerp import netsvc, tools, pooler
from openerp.osv import fields,orm
from openerp.tools.translate import _

import collections
import os
import zipfile
from math import log

def sizeof_fmt(num):
    unit_list = zip(['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'], [0, 0, 1, 2, 2, 2])
    """Human friendly file size"""
    if num > 1:
        exponent = min(int(log(num, 1024)), len(unit_list) - 1)
        quotient = float(num) / 1024**exponent
        unit, num_decimals = unit_list[exponent]
        format_string = '{:.%sf} {}' % (num_decimals)
        return format_string.format(quotient, unit)
    if num == 0:
        return '0 bytes'
    if num == 1:
        return '1 byte'
    
def zipdir(name, path):
    zipf = zipfile.ZipFile('/var/tmp/{0}.zip'.format(name), 'w')
    for root, dirs, files in os.walk(path):
        for f in files:
            zipf.write(os.path.join(root, f))
    
    return '/var/tmp/{0}.zip'.format(name)

def convert(data):
    if isinstance(data, basestring):
        return data.encode('utf-8')
    elif isinstance(data, collections.Mapping):
        return dict(map(convert, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(convert, data))
    else:
        return data
    
def format_download_name(string):
    
    string = string.replace('.', '_').replace(' ', '_')
    try:
        return string.encode('utf-8')
    except:
        return string

def reload_browser():
    return {
            'type': 'ir.actions.client',
            'tag': 'reload'
    }

def return_download_url(base64_string):
    """
    Returns the download link for a file encoded in base64.
    @param base64_string: The string resulting of encoding the file contents in base64.
    """
    return {
            'name'     : 'Download',
            'res_model': 'ir.actions.act_url',
            'type'     : 'ir.actions.act_url',
            'target'   : 'current',
            'url'      : 'data:application/octet-stream;base64;fileName=filename.txt,{0}'.format(base64_string)
    }
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: