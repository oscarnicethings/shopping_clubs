# -*- coding: utf-8 -*-
########################
# Created on 29/9/2014
#
# @author: openerp
########################

import openerp
import logging

from openerp import netsvc, tools, pooler
from openerp.osv import fields, orm
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class manufacturing(orm.Model):
    '''
    classdocs
    '''
    
    _name = 'manufacturing'
    
    _columns = {
        'manufacturing_type': fields.many2one('manufacturing.type', 'Type', required=True),
        'supplier': fields.many2one('res.partner', 'Supplier', required=True),
        'manufacturing_cost': fields.float('Manufacturing cost', digits_compute=dp.get_precision('Product Price'), help="Cost of production.", required=True),
    }
    
manufacturing()

class manufacturing_type(orm.Model):
    
    _name = 'manufacturing.type'
    
    _columns = {
        'name': fields.char('Name', size=20, required=True),
        'description': fields.text('Description'),
        'supplier_ids': fields.many2many('res.partner', 'supplier_manufacturing_rel', 'supplier_id', 'manufacturing_id', 'Manufacturing'),
    }
  
manufacturing_type()

class manufacturing_process(orm.Model):
    
    _name = 'manufacturing.process'
    
    _columns = {
        'manufacturing_type': fields.many2one('manufacturing.type', 'id', 'Type', required=True),
        'manufacturing_cost': fields.float('Manufacturing cost', digits_compute=dp.get_precision('Product Price'), help="Cost of production.", required=True),
        'supplier_id': fields.many2one('res.partner', 'Supplier', required=True, ondelete='cascade', select=True)
    }
    
manufacturing_process()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
