# -*- coding: utf-8 -*-
########################
# Created on 20/8/2014
#
# @author: Oscar Blanco
########################

import openerp
import logging

from openerp import netsvc, tools, pooler
from openerp.osv import fields,orm
from openerp.tools.translate import _

import json

_logger = logging.getLogger(__name__)

class club(orm.Model):
    '''
    Extends res_partner. Contains shopping clubs related to the company
    '''
    
    _inherit = 'res.partner'
    
    def _get_default_is_company(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
    
        if context.get("partner_type") == "shopping_club":
            return True
        else:
            return False       
    
    _columns = {
        'is_shopping_club': fields.boolean('Is a shopping club?', help="Check if the contact is a shopping club"),
        'ex_work': fields.boolean('Ex-work', help="This partner collects goods in Spain"),
        'shopping_club_category': fields.many2one('category', 'Club category'),
        'campaigns_clubs_rel': fields.one2many('campaigns.clubs.rel', 'id_club', 'Selling campaigns'),

    }
    
club()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: