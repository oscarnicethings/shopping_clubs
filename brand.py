# -*- coding: utf-8 -*-
########################
# Created on Apr 9, 2015
#
# @author: openerp
########################

import openerp
import logging

from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning

class Brand(models.Model):
    '''
    classdocs
    '''
    
    _name = 'brand.brand'
    
    name = fields.Char(size = 25, required = True)
    image = fields.Binary(string = 'Image')
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: