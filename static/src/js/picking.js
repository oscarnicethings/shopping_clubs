(function($) {
	
	$.lockPicking = function(html)
    {
    	$('body').append('<iframe id="toPrint"/><div id="pickingBlock">' + html + '</div>');
    }
	
	$.unlockPicking = function()
	{
		$('#pickingBlock').remove();
		$('#toPrint').remove();
	}
	
	$.showMessage = function(msg)
	{
		if (msg != '') {
			$('body').append(
					'<div id="alertMsg" style="">' +
					'<i class="fa fa-exclamation-triangle" style="color:#DC5F59;"></i>' +
					msg +
					'<br /><br />' +
					'<div style="text-align:center"><a href="#" style="display:inline-block;maring:0 auto;">Close</a></div>' +
					'</div>'
			);
		
			$div = $('#alertMsg');
			$close = $('#alertMsg a');
			$div.css({
				position:'fixed',
				zIndex:'2000',
				padding:'20px',
				background:'#DCDCDC',
				border:'1px solid rgba(0,0,0,0.5)',
				top: '50%',
				left: '50%',
				marginLeft: function() {return -$div.width()/2;},
				marginTop: function() {return -$div.height()/2;},
			});
			
			$close.css({
				padding: '4px 5px',
				textDecoration: 'none',
				borderRadius: '3px',
				background: '#DC5F59',
				border: '1px solid rgba(0,0,0,0.4)',
				margin: '0 3px',
				color: '#FFF',
			});
			$close.click(function() {$("#alertMsg").remove();})
		}
	}
})(jQuery);

openerp.shopping_clubs = function(instance, local) {
    var _t = instance.web._t,
        _lt = instance.web._lt;
    var QWeb = instance.web.qweb;
   
    jQuery.fn.flash = function( final_color, flash_color, duration )
    {
        this.animate( { backgroundColor: flash_color }, duration / 2 );
        this.animate( { backgroundColor: final_color }, duration / 2 );
    }
    
    //local.WidgetPicking = instance.web.Widget.extend({
    local.WidgetPicking = instance.web.form.AbstractField.extend({
    	className: 'oe_picking',
    	events: {
    		"click #open_picking": "startPicking",
    		"click #add_pallet": "startPicking",
    		"click #change_mode": "changeMode",
    		"click .print_pallet": "printPallet",
    		"click .pallet_button": "openPallet"
    	},
    	init: function(parent, context){
    		console.log(parent['$pager']);
    		console.log(context);
    		this._super(parent, context);
    		this.sod_id			= null;
    		this.SOD			= null;
    		this.line_ids		= null;
    		this.line_details	= null;
    		this.order_status	= null;
    		this.line_objects	= [];
    		this.picking		= false;	// When a pallet is added, picking is true. While picking the user can't add other pallet.
    		this.pallets		= [];
    		this.pallet_ids		= [];
    		this.working_pallet	= null;		// Actual pallet working with
    		this.pckage			= null;		// Actual package working with
    		this.mode			= 'add';
    		this.actions_binded	= false;
    		var self = this;
    		
    		//console.log(context);
    		console.log(this.getParent());
    		console.log(context);
    		self.SOD = new instance.web.Model(this.getParent(), 'supplier.order.details');
    		console.log(self.SOD);
    		self.sod_id = self.SOD.name.dataset.ids[self.SOD.name.dataset.index];

    	},
    	start: function() {
    		var ctx = new instance.web.CompoundContext();
    		var sup = this._super();
    		var self = this;
    		var deferred = []
    		
    		$('.oe_view_manager_switch').hide();
    		$('.oe_view_manager_pager').hide();
    		
    		deferred.push(
	    		new instance.web.Model('supplier.order.details')
								.query(['ccrel_id', 'pallets', 'line_details', 'status'])
								.filter([['id', '=', self.sod_id]])
								.limit(1)
								.first().then(function (details) {
									self.order_status = details.status;
									self.line_ids = details.line_details;
						    		
								})
			);
    		
    		$.when.apply($, deferred).then(function() {
    			console.log(self.pallets);
    			if (self.order_status === 'pending') self.statusPending();
	    		if (self.order_status === 'in_progress') self.statusInProgress();
	    		if (self.order_status === 'completed') self.statusCompleted();
    		});
		},
		isPicking: function() {
			return this.picking;
		},
		statusPending: function() {
			console.log('Pending');
			// If status is pending show the button to start picking.
			this.$el.append(QWeb.render("PendingTemplate", {widget: this}));
		},
		statusInProgress: function() {
			console.log('Progress');
			var self = this;
			var deferred = [];
			deferred.push(
		    		new instance.web.Model('supplier.order.pallet')
									.query(['id', 'reference', 'packages'])
									.filter([['sod_id', '=', self.sod_id]])
									.limit()
									.all().then(function (records) {
										$.each(records, function(index, record) {
											self.pallet_ids.push({'id':record.id, 'ref':record.reference});
											self._loadPallet(record);
										});
									})
			);
	    		
    		return $.when.apply($, deferred).then(function() {
    			self.$el.append(QWeb.render("InProgressTemplate", {widget: self}));
    		});
		},
		statusCompleted: function() {
			console.log('Completed');
			this.statusInProgress();
			
			$('#picking_form').remove();
			$('#change_mode').remove();
		},
		startPicking: function() {
			var self = this;
			
			$.when(self._get_line_details()).done(function() {
				$.when(self.showPickingUI()).done(function() {self._addPallet();});
			});
			
		},
		changeTemplate: function() {
			var self = this;
			self.$el.empty();
			self.$el.append(QWeb.render("InProgressTemplate", {widget: self}));
		},
		changeMode: function() {
			console.log('changing mode');
			var $text = $('#change_mode > span');
			var $icon = $('#change_mode > i');			
			if (this.mode == 'add') {
				this.mode = 'remove';
				$icon.switchClass('fa-arrow-up', 'fa-arrow-down');
				$text.text('Removing ');
			} else {
				this.mode = 'add';
				$icon.switchClass('fa-arrow-down', 'fa-arrow-up');
				$text.text('Adding ');
			}
		},
		changePickingStatus: function() {
			this.picking = !this.picking;
		},
		changePallet: function(pallet) {
			this.working_pallet.close();
			this.working_pallet = pallet;
		},
		showPickingUI: function() {
			var self = this;
			return $.when($.lockPicking(
							QWeb.render("StartPickingTemplate", 
							{widget: this,
							lines: self.line_details})
					)).done(function() {
						if (self.order_status != 'completed')
							self.picking = true;
						self._bindPickingActions();
					});
		},
		hiddePickingUI: function() {
			var self = this;
			
			if ((self.pallets.length > 0 || self.pallets_ids > 0) && self.order_status == 'pending') {
				self.order_status = 'in_progress';
				var model = new instance.web.Model('supplier.order.details');
	    		return model.call('write', [self.sod_id, {'status':'in_progress'}])
	    				.then(function () {
	    					$.unlockPicking();
	    					self.picking = false;
	    					self.do_action({
	    						type: 'ir.actions.act_window',
	    						res_id: self.sod_id,
	    						res_model: "supplier.order.details",
	    						views: [[false, 'form']],
	    						target: 'current',
	    						context: {},
	    					});
	    					return true;
	    				});
			} else {
				$.unlockPicking();
				self.picking = false;
				self.do_action({
					type: 'ir.actions.act_window',
					res_id: self.sod_id,
					res_model: "supplier.order.details",
					views: [[false, 'form']],
					target: 'current',
					context: {},
				});
				return true;
			}
		},
		/**
		 * Removed. Line qty's now is a calculated field.
		updateLines: function() {
			var self = this;
			$.each(self.line_objects, function(index, line) {
				line.save();
			});
		},
		**/
		printPallet: function(e) {
			$.when($.lockPicking('<span>Printing...</span>')).done(function() {
	    		url = '/report/pdf/shopping_clubs.report_so_pallet_details/' + e.currentTarget.id;
	    		//var model = new instance.web.Model('supplier.order.package');
	    		$iframe = $('iframe#toPrint');
				$iframe.attr('src', url);
				 
				$iframe.load(function() {
				    this.focus();
				    this.contentWindow.print();
				    $.unlockPicking();
				});
			});
			
			console.log(e.currentTarget.id);
		},
		openPallet: function(e) {
			console.log(e);
			var self = this;
			var pallet_id = e.currentTarget.id;
			console.log(self.pallets);
			
			$.when(self._get_line_details()).done(function() {
				$.when(self.showPickingUI()).done(function() {
					console.log('View loaded');
					$.when($.each(self.pallets, function(index, pallet) {
						console.log('Pallet id %s', pallet.getId());
						console.log('Target id %s', pallet_id);
						if (pallet.getId() == pallet_id) {
							console.log('Append pallet');
							self.working_pallet = pallet;
							self.working_pallet.appendTo($('#actual_picking'));
							self.working_pallet.showPackages();
							return true;
						}
					})).done(function() {
						self.picking = true;
						return true;
					});
					//self._addPallet();
				
				});
			});
			console.log(e.currentTarget.id);
		},
		removePallet: function() {
			var self = this;
			var to_remove = self.pallets.pop();
			
			to_remove.destroy();
		},
		// Add pallets, packages...
		_addPallet: function() {
			var self = this;
			console.log('added pallet');
			//if (self.picking) return; 										// Stop adding pallets if one is already opened
			//self.picking = true;

			$('#actual_picking').fadeOut('fast', function() {
				self.working_pallet = new local.WidgetPallet(self, self.pallets.length + 1, false, false, false);
				self.pallets.push(self.working_pallet);
				self.working_pallet.appendTo($('#actual_picking'));
			}).fadeIn('fast');
		},
		_loadPallet: function(pallet) {
			var self = this;
			self.pallets.push(new local.WidgetPallet(
					self, 
					self.pallets.length + 1,
					pallet.id,
					pallet.reference,
					pallet.packages));
		},
		_addPackage: function() {
			console.log('added package');
		},
		_closePackage: function() {
			var self = this;
			
			if (self.pckage != null) {
				
			}
		},
		_addTick: function(ean) {
			var self = this;
			ean = parseInt(ean);
			if (!self.picking) return;
			self._addQty(ean);
			self._updateView(ean, 'add');
		},
		_removeTick: function(ean) {
			console.log('meeeenos');
			var self = this;
			ean = parseInt(ean);
			if (!self.picking) return;
			if (self._removeQty(ean)) {
				console.log('ok');
				self._updateView(ean, 'remove');
				
			}
		},
		_addQty: function(ean) {
			var self = this;
			$.each(self.line_objects, function(index, line) {
				if (line.getEan() == ean) { 
					if (line.addOne()) self.working_pallet.pckage.addProduct(line.getProduct());// Add product on that line mapped to SODL
				}
			});
		},
		_removeQty: function(ean) {
			var self = this;
			var done = false
			$.each(self.line_objects, function(index, line) {
				if (line.getEan() == ean) {
					// First remove from package
					if (self.working_pallet.pckage.removeProduct(line.getProduct())) 
						if (line.removeOne())
							done = true;
				}
			});
			
			return done;
		},
		_updateView: function(ean, mode) {
			var self			= this;
			var $tr				= $('#' + ean);
			var $td_qty 		= $('#'+ ean + ' > .qty');
			var $td_picked		= $('#'+ ean + ' > .picked');
			var $td_name		= $('#'+ ean + ' > .name');
			var $td_remaining	= $('#'+ ean + ' > .remaining');
			var qty				= parseInt($td_qty.text());
			var qty_picked		= parseInt($td_picked.text());
			var remaining		= parseInt($td_remaining.text());
			var $container		= $('#picking_panel_container > div');

			if ($tr.length) {
				$container.animate({
					scrollTop: $tr.offset().top - $container.offset().top + $container.scrollTop()
			    }, 1000, function() {
			    	$tr.flash('#fff', '#dcdcdc', 1000);		    	
			    });

				if ((remaining > 0 && mode == 'add') || (qty_picked > 0 && mode == 'remove')) {
					mode == 'add' ? $td_picked.text(++qty_picked) : $td_picked.text(--qty_picked);
					mode == 'remove' ? $td_remaining.text(++remaining) : $td_remaining.text(--remaining);
				}
				qty_picked === qty ? $td_picked.css({background: '#428B24'}) :  $td_picked.css({background: '#fff'});
			}
		},
		_get_line_details: function() {
			var self = this;
			return new instance.web.Model('supplier.order.details.line')
					.query(['id', 'ean13', 'product', 'qty', 'qty_picked'])
					.filter([['id', 'in', self.line_ids]])
					.limit(self.line_ids.length)
					.all().then(function (details) {
						self.line_details = details;
						$.each(self.line_details, function(index, line) {
							self.line_objects.push(new SODL(line, index));
						});
					});
		},
		_bindPickingActions: function() {
			var self = this;
			var code = null;
			

			$('#ean_code').focus();
			$('#ean_code').blur(function() {
				$(this).focus();
			});
			
			// Called when the user 'picks' a product with the barcode scanner (?? picks ??)
			$('#picking_form').on('submit', function(e) {
				e.preventDefault();
				e.stopPropagation();
				if (this.order_status == 'completed') return false;
				this.code = $.trim($('#ean_code').val());
				$('#ean_code').val('');
				if (this.code) 
					if (self.mode == 'add')
						self._addTick(this.code);
					else
						self._removeTick(this.code);
				return false;
			});

			$('#change_mode').on('click', function(e) {

				self.changeMode();
			});
		},
		_unbindPickingActions: function() {
			$(document).off('#picking_form', 'submit');
			$('#change_mode').off();
		}
		
    });
    instance.web.form.widgets.add('picking','instance.shopping_clubs.WidgetPicking');
    //instance.web.form.custom_widgets.add('picking','instance.shopping_clubs.WidgetPicking');
    
    // PALLET WIDGET
    local.WidgetPallet = instance.web.Widget.extend({
    	template: 'PalletTemplate',
    	events: {
    		'click #add_package': 'addPackage',
    		'click #close_pallet': 'close',
    		'click .toggle_pallet': 'showPallet'
    		//'click .toggle_package': 'togglePackage',
    	},
    	init: function(parent, num, pallet_id, reference, package_ids, context) {
    		this._super(parent);
    		this.pallet_id		= pallet_id;	// Id of the record in database
    		this.packages		= [];
    		this.open			= true;
    		this.sod_id			= parent.sod_id;
    		this.pallet_num		= num;
    		this.pckage			= null;		// Actual package working with
    		this.reference		= reference;
    		
    		var deferred = []
    		var self = this;
    		
    		if (package_ids && package_ids.length > 0)
    			$.each(package_ids, function(index, p_id) {
    				self.packages.push(new local.WidgetPackage(self, p_id));
    				if (index == 0) self.pckage = self.packages[0];
    			});
    	},
    	start: function() {
    		this._super();
    		if (!this.pallet_id)
    			this.addPackage();
    	},
    	getId: function() {
    		var self = this;
    		if (!self.pallet_id) {
    			return $.when(self._save([]))
    					.then(function(pallet_id) {
    						$('#pallets_container a:last-child').before(
    								QWeb.render("PalletButtonTemplate", {obj: {'id': self.pallet_id, 'ref': self.reference}})
    						);
		    				return pallet_id;
		    			});
    		} else
    			return self.pallet_id;
    	},
    	isOpen: function() {return this.open;},
    	open: function() {this.open = true;},
    	close: function() {
    		var self = this;

    		$.each(self.packages, function(index, pckage) {
    			if (pckage.isOpen() && !pckage.isEmpty()) {
					$.showMessage('You must close/print all packages before closing this pallet');
					self.open = true;
					return false;
    			} else if (pckage.isEmpty()) {
    				self.packages.splice(index, 1);
    				pckage.destroy();
    			} else {
    				self.open = false;
    			}
    		});
    		
    		if (!self.isOpen()) {
	    		$.when(self.getId()).then(function(pallet_id) {
	    			self.open = false;
	    			$.when(self.save()).then(function() {
	    				self.print();
	    				console.log('Printing...');
	    				//$.when(self.getParent().updateLines()).done(function() {self.getParent().hiddePickingUI();});
	    				self.getParent().hiddePickingUI();
	    				$('#pallet_info_' + self.pallet_num).slideToggle('slow', function() {
	    					$('#picking_buttons').slideToggle('slow', function() {
	    						$('#toggle_pallet_' + self.pallet_num).next().animate({width: 'toggle'});
	    						self.getParent().changePickingStatus();
	    						
	    					});
	    				});
	    			});
	    		});
    		} else if (self.packages.length == 0) {
    			$.when(self.getParent().hiddePickingUI()).done(function() {
    				self.getParent().changePickingStatus();
    				self.getParent().removePallet();
    			});
    		}
    	},
    	showPallet: function() {
    		console.log('Changed pallet');
    		var self = this;
    		if (self.getParent().isPicking())
    			self.getParent().changePallet(self);
    		
    		$('#pallet_info_' + self.pallet_num).slideToggle('slow', function() {
				$('#picking_buttons').slideToggle('slow', function() {
					$('#toggle_pallet_' + self.pallet_num).next().animate({width: 'toggle'});
					self.getParent().changePickingStatus();
				});
			});
    	},
    	addPackage: function() {
    		var self = this;
    		// If pckage is not null, hide actual package
    		if (self.pckage != null && self.pckage.isEmpty()) return;
    		if (self.pckage != null) self.pckage.hide();

			self.pckage = new local.WidgetPackage(self, false);
    		self.pckage.setNum(self.packages.length + 1);
    		self.packages.push(self.pckage);
    		self.pckage.appendTo(self.$el.find('#packages'));
    	},
    	togglePackage: function(pckage) {
    		var self = this;
    		console.log('Working with %s', self.pckage.package_num);
    		//if (pckage.isEmpty()) return;
    		if (self.pckage != null) self.pckage.hide();
    		self.pckage = pckage;
    		self.pckage.show();
    	},
    	showPackages: function() {
    		var self = this;
    		$.each(self.packages, function(index, pckage) {
    			pckage.appendTo(self.$el.find('#packages'));
    			pckage.closePackage();
    		});
    		
    	},
    	addProduct: function(product) {
    		this.pckage.addProduct(product);
    	},
    	save: function() {
    		console.log('saving pallet...');
    		if (!navigator.onLine) {
    			alert('Connection error!\nPlease check your internet connection and try again.')
    			return;
    		}
    		var self = this;
    		var package_ids = [] // Package ids in database

    		$.each(self.packages, function(index, pckage) {
    			if (!pckage.isEmpty())
    				package_ids.push(pckage.getId());
    		});
    	
    		if (package_ids.length > 0) 
    			$.when(self._save(package_ids)).done(function() {return self.pallet_id;});
    	},
    	_save: function(package_ids) {
    		var self = this;
    		var model = new instance.web.Model('supplier.order.pallet');
    		return model.call('write_pallet', [self.sod_id, self.pallet_id, package_ids])
    				.then(function (data) {
    					self.pallet_id = parseInt(data.id);
    					self.reference = data.reference;
    					return self.pallet_id;
    				});
    	},
    	// TODO merge print functions
    	print: function() {
    		var self = this;
    		
    		if (!self.pallet_id) alert('Could not get pallet id!');
    		url = '/report/pdf/shopping_clubs.report_so_pallet_details/' + self.pallet_id;
    		//var model = new instance.web.Model('supplier.order.package');
    		$iframe = $('iframe#toPrint');
			$iframe.attr('src', url);
			 
			$iframe.load(function() {
			    this.focus();
			    this.contentWindow.print();
			});
    	}
    });
    
    local.WidgetPackage = instance.web.Widget.extend({
    	template: 'PackageTemplate',
    	events: {
    		'click .save_package': 'save',
    		'click .empty_package': 'empty',
    	},
    	init: function(parent, pckage_id, context) {
    		this._super(parent);
    		this.package_id		= pckage_id;
    		this.open			= true; 
    		this.pallet			= parent; // Instance of pallet parent widget
    		this.package_num	= parseInt(parent.packages.length) + 1;
    		this.products		= [];
    		this.reference		= false;
    		var deferred 		= []
    		var self 			= this;
    		
    		if (pckage_id) {
    			self._loadReference();
    			self._loadProducts();
    		}
    	},
    	start: function() {
    		this._super();
    		var self = this;
    		
    		this.getParent().$('#toggle_package_' + this.package_num).click(function(){
    			self.changePackage();
    		});
    		
    		this.getParent().$('#save_package_' + this.package_num).click(function(){
    			self.closeAndPrint();
    		});
    	},
    	getId: function() {
    		var self = this;
    		if (!self.package_id) {
    			return $.when(self.save())
    					.then(function(package_id) {
		    				return pacakge_id;
		    			});
    		} else
    			return self.package_id;
    	},
    	isEmpty: function() {return this.products.length == 0;},
    	isOpen: function() {return this.open;},
    	openPackage: function() {
    		this.open = true;
    		this.getParent().$('#save_package_' + this.package_num).switchClass( "green_button", "red_button", 1000, "easeInOutQuad" );
    	},
    	closePackage: function() {
    		this.open = false;
    		this.getParent().$('#save_package_' + this.package_num).switchClass( "red_button", "green_button", 1000, "easeInOutQuad" );
    	},
    	changePackage: function() {
    		var self = this;
    		self.getParent().togglePackage(self);
    	},
    	closeAndPrint: function() {
    		var self = this;
    		
    		if (!navigator.onLine) {
    			alert('Connection error!\nPlease check your internet connection and try again.')
    			return;
    		}
    		if (self.isEmpty()) {
    			$.showMessage('You cannot close/print an empty package.');
    			return false;
    		}
    		$.when(self.save()).then(function(value) {
    			self.print();
    			self.closePackage();
    		});
    	},
    	hide: function() {
    		var self = this;
    		// Save package before hidding
    		self.save();
    		self.getParent().$('#package_' + self.package_num).fadeOut('slow');
    	},
    	show: function() {
    		var self = this;
    		self.getParent().$('#package_' + self.package_num).fadeIn('slow');
    	},
    	save: function() {
    		var self = this;
    		console.log('saving package... %s', this.package_num);    		
    		if (self.isEmpty()) return;

    		return $.when(self.getParent().getId())
    					.then(function(pallet_id) {
    						return self._save(pallet_id);
    					});
    	},
    	_save: function(pallet_id) {
    		var self = this;
    		var model = new instance.web.Model('supplier.order.package');
    		return model.call('write_package', [pallet_id, self.package_id, self.products])
    				.then(function (data) {
    					self.package_id = parseInt(data.id);
    					return self.package_id;
    				});
    	},
    	_loadReference: function() {
    		var self = this;
    		if (self.package_id) {
    			new instance.web.Model('supplier.order.package.product')
	    		.query(['reference'])
				.filter([['package_id', '=', self.package_id]])
				.limit(1)
				.first().then(function (record) {
					self.reference = record.reference;
				});
    		}
    	},
    	_loadProducts: function() {
    		var self = this;
    		var deferred = [];
    		
    		if (self.package_id) {
    			new instance.web.Model('supplier.order.package.product')
		    		.query(['product', 'ean13', 'qty'])
					.filter([['package_id', '=', self.package_id]])
					.limit()
					.all().then(function (records) {
						
						$.each(records, function(index, record) {
							product_obj = new Product(record.product[0], record.product[1], record.ean13, record.qty);
			    			self.products.push(product_obj);
						});
					});
    		}
    	},
    	// TODO merge print functions
    	print: function() {
    		var self = this;
    		
    		if (!self.package_id) alert('Could not get package id!');
    		console.log(window.location.origin);
    		//url = 'http://erp.reallynicethings.es:8069/report/pdf/shopping_clubs.report_so_package_details/' + self.package_id;
    		url = '/report/pdf/shopping_clubs.report_so_package_details/' + self.package_id;
    		//var model = new instance.web.Model('supplier.order.package');
    		$iframe = $('iframe#toPrint');
			$iframe.attr('src', url);
			 
			$iframe.load(function() {
			    this.focus();
			    this.contentWindow.print();
			});
    	},
    	empty: function(e) {
    		console.log('Not implemented');
    	},
    	setNum: function(package_num) {
    		var self = this;
    		self.num = package_num;
    	},
    	addProduct: function(prd) {
    		var self = this;
    		var product_obj = false;
    		$.each(self.products, function(index, product) {
    			if (product.getId() === prd.getId()) {
    				product.addQty();
    				product_obj = product;
    			} 
    		});
    		
    		if (product_obj === false) {
    			product_obj = new Product(prd.getId(), prd.getName(), prd.getEan(), 1);
    			self.products.push(product_obj);
    		}
    		self._updateView(product_obj);
    	},
    	removeProduct: function(prd) {
    		var self = this;
    		product_obj = false;
    		$.each(self.products, function(index, product) {
    			if (product.getId() ===  prd.getId()) {
    				product.removeQty();
    				if (product.getQty() == 0) {
    					self.products.splice(index, 1);
    				}
    				product_obj = product;
    				return false;
    			}
    		});
    		
    		if (product_obj) {
    			self._updateView(product_obj);
    			return true;
    		} else
    			return false;
    	},
    	_updateView: function(product_obj) {
    		var self = this;
    		$p_row = self.$el.find('#product_' + parseInt(product_obj.getId()));
    		if ($p_row.length > 0) {
				if (product_obj.getQty() == 0)
					$p_row.remove();
				else
					$p_row.find('td:last').text(product_obj.getQty());
			} else {
				self.getParent().$('#package_' + self.package_num).append(
					'<tr id="product_' + product_obj.getId() + '"><td colspan="3">' + product_obj.getName() + '</td><td colspan="1">' + product_obj.getQty() + '</td></tr>'
				);
			}
    		if (!self.isOpen()) self.openPackage();
    	},
    });
    //////////////////////////////////////////////////////////////////////    
    // CLASSES ///////////////////////////////////////////////////////////
    // supplier_order_details_line
    var SODL = instance.web.Class.extend({
     	
    	init: function(line, index) {
    		// Mapped to supplier_order_details_line
    		// Index is the position of the sodl from the database, just for ensure data integrity
    		this.id			= line.id;			// line id
        	// this.product	= line.product;		
        	// this.ean13	= line.ean13;		// ean13
        	this.qty		= line.qty;			// qty
        	this.qty_picked	= line.qty_picked;	// qty_picked
        	this.index 		= line.index;
        	this.product 	= new Product(line.product[0], line.product[1], line.ean13, 1) // product Obj. [0] -> id, [1] -> name
        	this.model		= new instance.web.Model('supplier.order.details.line');
    	},
    	getProduct: function() {
    		return this.product;
    	},
    	getEan: function() {
    		var self = this;
    		return self.product.getEan();
    	},
    	getProductId: function() {
    		var self = this;
    		return self.product.getProductId();
    	},
    	getProductName: function() {
    		var self = this;
    		return self.product.getProductName();
    	},
    	addOne: function() {
    		var self = this;
    		if(self.qty_picked < self.qty) {
    			self.qty_picked++;
    			console.log('Picked one');
    			return true;
    		} else
    			return false;
    	},
    	removeOne: function() {
    		var self = this;
    		if (self.qty_picked > 0) {
    			self.qty_picked--;
    			return true;
    		} else    		
    			return false;
    	},
    	save: function() {
    		// TODO
    		// write object to server
    		var self = this;
    		return self.model
			    		 .call("write", [[self.id], {'qty_picked': self.qty_picked}])
					         .then(function () {
					        	 return true;
					        	 /**
					             self.do_load_reconciliation_proposition = false; // of the server might set the statement line's partner
					             self.animation_speed = 0;
					             return $.when(self.restart(self.get("mode"))).then(function(){
					                 self.do_load_reconciliation_proposition = true;
					                 self.is_consistent = true;
					                 self.set("mode", "match");
					             });
					             **/
					         });
    		return null;
    	}
    	
	});
    
    var Product = instance.web.Class.extend({
    	init: function(id, name, ean, qty) {
    		this.product_id		= id;	//line.getProductId();
    		this.product_name	= name; //line.getProductName();
    		this.ean13			= ean;	//line.getEan();
    		this.qty	 		= qty;
    	},
    	addQty: function() {
    		this.qty++;
    	},
    	removeQty: function() {
    		this.qty--;
    	},
    	getId: function() {
    		return this.product_id;
    	},
    	getName: function() {
    		return this.product_name;
    	},
    	getEan: function() {
    		return parseInt(this.ean13);
    	},
    	getQty: function() {
    		return this.qty;
    	}
    });
}