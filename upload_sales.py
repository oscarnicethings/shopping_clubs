# -*- coding: utf-8 -*-
########################
# Created on 10/9/2014
#
# @author: openerp
########################

import openerp
import logging
from tools import reload_browser

from openerp import netsvc, tools, pooler
from openerp.osv import fields,orm,osv
from openerp.tools.translate import _

import csv
import base64
from StringIO import StringIO

from csv_mapping import sales_positions

_logger = logging.getLogger(__name__)


class upload_sales(osv.osv_memory):
    '''
    Upload a file with sales result for a selling campaign
    '''

    _name = 'upload.sales'
    _columns = {
        'sales_file': fields.binary('Sales file'),
        'skip_first': fields.boolean('Skip first line'),
        'use_old_reference': fields.boolean('Use old reference?'),
        'separator': fields.selection(((',', ','),
                                       (';', ';'),
                                       ('|', '|')), 'Separator'),
    }
    
    
    def upload_sales_results(self, cr, uid, ids, context=None):
        res = self.read(cr, uid, ids,['sales_file', 'skip_first', 'use_old_reference', 'separator'])
        res = res and res[0] or {}
        ccrel_id = context.get('ccrel_id')

        product_obj = self.pool.get('product.product')
        sod_obj = self.pool.get('supplier.order.details')
        sodl_obj = self.pool.get('supplier.order.details.line')
        sales_line_obj = self.pool.get('sales.line')
        _skip = res['skip_first']
        _ref = 'default_code' if res['use_old_reference'] else 'product_code' 
        for row in StringIO(base64.decodestring(res['sales_file'])):
            if _skip == True:
                _skip = False
                continue
            fields = row.split(res['separator'].encode('utf-8'))
            #p_id = product_obj.search(cr, uid, [('default_code','=', fields[sales_positions['supplier_ref']])])
            p_id = product_obj.search(cr, uid, [(_ref,'=', fields[sales_positions['ref']])])
            try:
                qty = int(fields[sales_positions['sales_qty']])
            except:
                qty = 0
            
            if fields[sales_positions['price']] and float(fields[sales_positions['price']].replace(',', '.')) > 0.0:
                try:
                    price = float(fields[sales_positions['price']].replace(',', '.'))
                except:
                    raise osv.except_osv(
                            _('Error!'),
                            _('Check price value for product with reference {0}.'.format(fields[sales_positions['ref']])))
            else:
                price = False
                
            line_id = sales_line_obj.search(cr, uid, [('product','in',p_id), ('ccrel_id','=',ccrel_id)])
            
            if line_id:
                if price:
                    sales_line_obj.write(cr, uid, line_id, {'qty': qty, 'campaign_price': price})
                else:
                    sales_line_obj.write(cr, uid, line_id, {'qty': qty})
                
            ## Search for all supplier_order_lines with same ccrel_id, foreach line check if product is details and update qty
            sod_ids = sod_obj.search(cr, uid, [('ccrel_id', '=', ccrel_id)])
            sod_obj.unlink(cr, uid, sod_ids)
            """
            sodl_ids = sodl_obj.search(cr, uid, [('sod_id', 'in', sod_ids)])
            for sodl in sodl_obj.browse(cr, uid, sodl_ids):
                if len(p_id) > 0 and sodl.product.id == p_id[0]:
                    sodl.write({'qty': qty})
            """
            #else:
            #    sales_line_obj.create(cr, uid, {'product': p_id[0], 'qty': qty, 'ccrel_id': ccrel_id, 'campaign_price': fields[sales_positions['pvp']]})
            
        
        self.pool.get('campaigns.clubs.rel').write(cr, uid, ccrel_id, {'is_sales_uploaded':True})
        
        reload_browser()
        
upload_sales()