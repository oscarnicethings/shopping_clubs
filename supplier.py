# -*- coding: utf-8 -*-
########################
# Created on 30/9/2014
#
# @author: openerp
########################

import openerp
import logging

from openerp import SUPERUSER_ID
from openerp import netsvc, tools, pooler, api
from openerp.osv import fields, orm, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

from tools import convert, format_download_name

import csv, os, shutil
from StringIO import StringIO
from string import replace
from collections import OrderedDict
from tools import zipdir
from openerp.addons.shopping_clubs.tools import reload_browser
import collections

_logger = logging.getLogger(__name__)

class supplier(orm.Model):
    '''
    classdocs
    '''
    
    _inherit = 'res.partner'
    
    _columns = {
        'manufacturing_type': fields.many2many('manufacturing.type', 'supplier_manufacturing_rel', 'manufacturing_id', 'supplier_id', 'Manufacturing'),
        'manufacturing_cost': fields.one2many('supplier.manufacturing.cost', 'supplier_id', 'Manufacturing cost'),
        'supplier_orders': fields.one2many('supplier.order.details', 'supplier_id', 'Supplier orders'),
        'warehouse': fields.one2many('supplier.warehouse', 'supplier', 'Warehouse'),
        'has_irpf': fields.boolean('IRPF', help="Check if this supplier apply IRPF retention"),
        'vat': fields.char('TIN', help="Tax Identification Number. Check the box if this contact is subjected to taxes. Used by the some of the legal statements.", required=True),
    }

    def action_show_warehouse(self, cr, uid, ids, context=None):

        this = self.browse(cr, uid, ids[0])
        if not this.warehouse.id:
            w_id = self.pool.get('supplier.warehouse').create(cr, uid, {'supplier': ids[0]})
            this.write({'warehouse': [(4, w_id)]})
        return {
                'type': 'ir.actions.act_window',
                'res_model': 'supplier.warehouse',
                'view_mode': 'form',
                'target': 'new',
                'name': _('{0}\'s warehouse'.format(this.name.encode('utf-8'))),
                'res_id': this.warehouse.id
            }
    
    def action_show_supplier_price_list(self, cr, uid, ids, context=None):

        win_obj = self.pool.get('ir.actions.act_window')
        res = win_obj.for_xml_id(cr, uid, 'shopping_clubs', 'supplier_manufacturing_cost_action', context)
        res['target'] = 'new'
        res['domain'] = [('supplier_id', '=', ids[0])]
        return res
    
    def _write_smc_lines(self, cr, uid, ids, vals, context=None):
        _p_obj = self.pool.get('product.product')
        _smc_obj = self.pool.get('supplier.manufacturing.cost')
        if 'manufacturing_type' in vals.keys():
            # mt_id = vals.get('manufacturing_type')[0][2][0]
            for mt in vals.get('manufacturing_type'):
                for mt_id in mt[2]:
                    _products = _p_obj.search(cr, uid, [('manufacturing', '=', mt_id)])
                 
                    for _p_id in _products:
                        if not _p_obj.browse(cr, uid, _p_id).product_class.id:
                            continue
                        _query = [('supplier_id', '=', ids[0] if isinstance(ids, list) else ids),
                                 ('manufacturing_type_id', '=', mt_id),
                                 ('product_class_id', '=', _p_obj.browse(cr, uid, _p_id).product_class.id)]

                        if len(_smc_obj.search(cr, uid, _query)) < 1:
                            _smc_obj.create(cr, uid,
                                            {'supplier_id': ids[0] if isinstance(ids, list) else ids,
                                             'manufacturing_type_id': mt_id,
                                             'product_class_id': _p_obj.browse(cr, uid, _p_id).product_class.id,
                                             'manufacturing_cost': 0.0})
                        else:
                            _logger.info('what?')
            
        return True
        
                            
    def create(self, cr, uid, vals, context=None):
        record_id = super(supplier, self).create(cr, uid, vals, context)
        self._write_smc_lines(cr, uid, record_id, vals, context)
        return record_id
    
    def write(self, cr, uid, ids, vals, context=None):
        if 'manufacturing_type' in vals.keys():
            self._write_smc_lines(cr, uid, ids, vals, context)
        return super(supplier, self).write(cr, uid, ids, vals)
                                                   
supplier()

class supplier_manufacturing_cost(orm.Model):
    
    _name = 'supplier.manufacturing.cost'
    
    _columns = {
        'supplier_id': fields.many2one('res.partner', 'Supplier', ondelete='cascade', required=True),
        'manufacturing_type_id': fields.many2one('manufacturing.type', 'Manufacturing type', ondelete='cascade', required=True),
        'product_class_id': fields.many2one('product.class', 'Product class', ondelete="cascade", required=True),
        'manufacturing_cost': fields.float('Manufacturing cost', digits_compute=dp.get_precision('Product Price'), help="Cost of production.", required=True),
    }
    
    _sql_constraint = [('unique_mcost', 'unique(supplier_id, manufacturing_type_id, product_class_id)', 'Error! Product Name Already Exist!')]
    
supplier_manufacturing_cost()

class supplier_order_details(orm.Model):
    
    _name = 'supplier.order.details'
    _inherit = ['mail.thread']
    _rec_name = 'reference'
    
    def _get_purchase_order(self, cr, uid, ids, field, args, context=None):
        return dict([(_id, self.browse(cr, uid, _id).ccrel_id.purchase_order) for _id in ids ])
    
    def _get_campaign_number(self, cr, uid, ids, field, args, context=None):
        return dict([(_id, self.browse(cr, uid, _id).ccrel_id.campaign_number) for _id in ids ])
    
    def _get_campaign_name(self, cr, uid, ids, field, args, context=None):
        return dict([(_id, self.browse(cr, uid, _id).ccrel_id.name) for _id in ids ])
    
    def _get_campaign_ref(self, cr, uid, ids, field, args, context=None):
        return dict([(_id, self.browse(cr, uid, _id).ccrel_id.id_campaign.ref) for _id in ids ])
    
    def _get_units_count(self, cr, uid, ids, field, args, context=None):
        return  dict([(_id, sum([line.qty for line in self.browse(cr, uid, ids).line_details])) for _id in ids ])
    
    def _get_reference(self, cr, uid, ids, field, args, context=None):
        res = {}
        
        for _id in ids:
            res[_id] = '{0:06d}{1:02d}'.format(_id, self.browse(cr, uid, _id).supplier_id.id)
        return res
            
    _columns = {
        'file_name': fields.char('File name', size=50),
        'file_stream': fields.binary('CSV file', readonly=True),
        'ccrel_id': fields.many2one('campaigns.clubs.rel', 'Selling campaign', ondelete='cascade', required=True),
        'supplier_id': fields.many2one('res.partner', 'Supplier', ondelete='cascade', required=True),
        'line_details': fields.one2many('supplier.order.details.line', 'sod_id', 'Order line details.'),
        'pallets': fields.one2many('supplier.order.pallet', 'sod_id', 'Pallets'),
        'purchase_order': fields.function(_get_purchase_order, type='char',  string="Purchase order"),
        'campaign_number': fields.function(_get_campaign_number, type='char',  string="Campaign number"),
        'campaign_name': fields.function(_get_campaign_name, type='char',  string="Campaign name"),
        #'campaign_ref': fields.function(_get_campaign_ref, type='char',  string="Campaign number"),
        'reference': fields.function(_get_reference, type='char',  string="Reference number"),
        'total_units': fields.function(_get_units_count, type='integer',  string="Total units"),
        'is_invoiced': fields.boolean('Is invoiced', help="If this order is already invoiced."),
        'invoice': fields.many2one('account.invoice', 'Invoice', ondelete='restrict'),
        'pack_individual': fields.related('ccrel_id', 'pack_individual', type='boolean', string='Pack individual', readonly=True),
        'delivery_date': fields.related('ccrel_id', 'delivery_date', type='date', string='Delivery date', readonly=True),
        'manufacturing': fields.many2many('manufacturing.type', 'order_manufacturing_rel', 'order_id', 'manufacturing_id', 'Manufacturing'),
        'order_type': fields.selection([('purchase', 'Purchase'),
                                        ('manufacturing', 'Manufacturing')], string='Order type', default='purchase'),
        'status': fields.selection([('waiting', 'Waiting for confirmation'),
                                    ('pending', 'Pending'),  
                                    ('in_progress', 'In Progress'),
                                    ('completed', 'Completed')], string='Status', default='waiting', index=True, readonly=True)
    }
    
    def action_donwload_invoice(self, cr, uid, ids, context=None):
        
        assert len(ids) == 1, 'This action should only be used with one id at time.'
        return self.pool['report'].get_action(cr, uid, [self.browse(cr, uid, ids).invoice.id], 'account.report_invoice', context=context)
        
    def action_generate_invoice(self, cr, uid, ids, context=None):
        
        def _check_cost_price(lines):
            for line in lines:
                if float(line.product.standard_price) == 0.0:
                    raise osv.except_osv(_('Warning!'),
                                         _('The product {0} with product code {1} has no cost price. Please check it.'.format(line.product.name.encode('utf-8'), line.product.standard_price)))
            
            return True
        
        def _check_product_classes(lines):
            for line in lines:
                if line.product.product_class is False:
                    raise osv.except_osv(_('Warning!'),
                                         _('The product {0} with product code {1} has no product class. Please check it.'.format(line.product.name.encode('utf-8'), line.product.product_code)))
            
            return True
        
        assert len(ids) == 1, 'This action should only be used with one id at time.'
        this = self.browse(cr, uid, ids)
        
        if not this.is_invoiced:
            invoice_lines = []
            invoice_line_obj = self.pool.get('account.invoice.line')
            invoice_id = self.pool.get('account.invoice').create(cr, uid, {'origin': this.reference,
                                                                           'partner_id': this.supplier_id.id,
                                                                           'account_id': 431,
                                                                           'type': 'in_invoice'})
            if this.order_type == 'purchase':
                
                if _check_cost_price(this.line_details):
                    for line in this.line_details:
                        if line.qty_picked > 0:
                            invoice_lines.append(invoice_line_obj.create(cr, uid, {'product_id': line.product.id,
                                                                                  'name': line.product.name.encode('utf-8'),
                                                                                  'invoice_id': invoice_id,
                                                                                  'price_unit': line.product.standard_price,
                                                                                  'quantity': line.qty_picked,
                                                                                  'invoice_line_tax_id': [(6, 0, [1])]}))
            else:
                _smc_obj = self.pool.get('supplier.manufacturing.cost')
                ## Get manufacturing cost per line - search(supp_id, manu_id, class_id)
                if _check_product_classes(this.line_details):
                    _smc_ids = []
                    for line in this.line_details:
                        if line.qty_picked > 0:
                            _smc_ids = [_smc_obj.search(cr, uid, [('supplier_id', '=', this.supplier_id.id), 
                                                                 ('manufacturing_type_id', '=', _m_type.id),
                                                                 ('product_class_id', '=', line.product.product_class.id)])[0] for _m_type in this.manufacturing]
                            
                            for _smc_id in _smc_ids:
                                _smc =  _smc_obj.browse(cr, uid, _smc_id)
                                if _smc:
                                    invoice_lines.append(invoice_line_obj.create(cr, uid, {'product_id': line.product.id,
                                                                                           'name': '{0} - {1}'.format(line.product.name.encode('utf-8'), 
                                                                                                                     _smc.manufacturing_type_id.name.encode('utf-8')),
                                                                                           'invoice_id': invoice_id,
                                                                                           'price_unit': _smc.manufacturing_cost,
                                                                                           'quantity': line.qty_picked,
                                                                                           'invoice_line_tax_id': [(6, 0, [1])]}))
                  
            self.write(cr, uid, this.id, {'is_invoiced': True, 'invoice': invoice_id})
        
    def action_generate_labels(self, cr , uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time'
        return self.pool['report'].get_action(cr, uid, ids, 'shopping_clubs.report_shopping_clubs_labels', context=context)
    
    def action_generate_labels_b8(self, cr , uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time'
        return self.pool['report'].get_action(cr, uid, ids, 'shopping_clubs.report_shopping_clubs_labels_b8', context=context)
    
    def action_generate_report(self, cr , uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time'
        return self.pool['report'].get_action(cr, uid, ids, 'shopping_clubs.report_supplier_order_details', context=context)
    
    def action_generate_csv(self, cr , uid, ids, context=None):
        _sodl = self.browse(cr, uid, ids).line_details
        this = self.browse(cr, uid, ids)
        irattachment = self.pool.get('ir.attachment')
        
        """
        rows = [OrderedDict({'Name': l.product.name, 
                             'Product code': l.product.product_code,
                             'Old reference': l.product.default_code,
                             'Class': l.product.product_class.name,
                             'Height': l.product.height,
                             'Width': l.product.width,
                             'Length': l.product.length,
                             'Quantity': '{0}'.format(l.qty)}) for l in _sodl]
        """
        rows = []
        for l in _sodl:
            row = OrderedDict()
            row[_('Name')] = l.product.name
            row[_('Product code')] = l.product.product_code
            row[_('Old reference')] = l.product.default_code
            row[_('Class')] = l.product.product_class.name
            row[_('Height')] = l.product.height
            row[_('Width')] = l.product.width
            row[_('Length')] = l.product.length
            row[_('Quantity')] = '{0}'.format(l.qty)
            rows.append(row)
            
        cso = StringIO()
        csv_writer = csv.DictWriter(cso, rows[0].keys(), delimiter=';')
        csv_writer.writeheader()
        csv_writer.writerows(convert(rows))
        
        ir_id = irattachment.search(cr, uid, [('res_model', '=', 'supplier.order.details'), ('res_id', '=', ids[0]), ('name', 'like', '%.csv')])
        if ir_id:
            irattachment.write(cr, uid, ir_id, {'db_datas': cso.getvalue().encode('base64')})
        else:
            _c_name = this.ccrel_id.id_campaign.name if this.ccrel_id.id_campaign else this.ccrel_id.name
            file_name = '{0}_{1}_{2}.csv'.format(this.order_type, format_download_name(this.supplier_id.name), format_download_name(_c_name))
            ir_id = self.pool.get('ir.attachment').create(cr, uid, 
                                                  {'name': file_name,
                                                   'datas_fname': file_name,
                                                   'db_datas': cso.getvalue().encode('base64'),
                                                   'type': 'binary',
                                                   'res_model': 'supplier.order.details',
                                                   'res_id': ids[0]})
      
        return {
                'name'     : 'Download',
                'res_model': 'ir.actions.act_url',
                'type'     : 'ir.actions.act_url',
                'target'   : 'current',
                'url'      : 'web/binary/saveas?model=ir.attachment&field=datas&filename_field=name&id={0}'.format(ir_id[0] if isinstance(ir_id, list) else ir_id)
        }
        
    def action_generate_originals(self, cr, uid, ids, context=None):
        _sodl = self.browse(cr, uid, ids).line_details
        this = self.browse(cr, uid, ids)
        
        if not os.path.exists('/var/tmp/tozip'):
            os.makedirs('/var/tmp/tozip')
            
        for _line in _sodl:
            _originals = _line.product.product_originals
            if _originals:
                for num, _original in enumerate(_originals):
                    if _original.image:
                        with open('/var/tmp/tozip/{0}_{1}_{2}_{3}.jpg'.format(_line.product.product_code,
                                                                              _line.product.name.encode('utf-8'),
                                                                              _line.product.product_class.name.encode('utf-8'),
                                                                              num), 'w+') as img:
                            img.write(_original.image.decode('base64'))
        
        if this.ccrel_id.id_campaign:
            _campaign_name = this.ccrel_id.id_campaign.name
        else:
            _campaign_name = this.ccrel_id.name
        _supplier_name = this.supplier_id.name
        zippath = zipdir('{0}_{1}'.format(_campaign_name.encode('utf-8'), _supplier_name.encode('utf-8')), '/var/tmp/tozip')
        with open(zippath, 'rb') as fff:
            attachment_csv = fff.read().encode('base64')
            #stream_csv = base64.b64encode(fff.read())
        
        try: 
            os.remove(zippath)
            shutil.rmtree('/var/tmp/tozip')
        except OSError:
            pass
        
        irattachment = self.pool.get('ir.attachment')
        ir_id = irattachment.search(cr, uid, [('res_model', '=', 'supplier.order.details'), ('res_id', '=', ids[0]), ('name', 'like', '%.zip')])
        if ir_id:
            irattachment.write(cr, uid, ir_id, {'db_datas': attachment_csv})
        else:
            ir_id = self.pool.get('ir.attachment').create(cr, uid, 
                                                  {'name': zippath.replace('/var/tmp/', ''),
                                                   'datas_fname': zippath.replace('/var/tmp/', ''),
                                                   'db_datas': attachment_csv,
                                                   'type': 'binary',
                                                   'res_model': 'supplier.order.details',
                                                   'res_id': ids[0]})  
        return {
                'name'     : 'Download',
                'res_model': 'ir.actions.act_url',
                'type'     : 'ir.actions.act_url',
                'target'   : 'current',
                'url'      : 'web/binary/saveas?model=ir.attachment&field=datas&filename_field=name&id={0}'.format(ir_id[0] if isinstance(ir_id, list) else ir_id)
        }
    
    def action_complete_order_admin(self, cr, uid, ids, context=None):
        
        this = self.browse(cr, uid, ids)          
        this.write({'status': 'completed'})
        return {
                'type': 'ir.actions.act_window',
                'res_model': 'supplier.order.details',
                'view_mode': 'form',
                'res_id': ids[0]
            }
        
    def action_complete_order(self, cr, uid, ids, context=None):
        
        def _compose_body(order):
            _body = _("""
                    Hi, {0}\n
                    the order {1} is ready to go.
                    """.format(order.create_uid.partner_id.name.encode('utf-8'), order.reference))
            return _body
        
        this = self.browse(cr, uid, ids)         
        for line in this.line_details:
            if int(line.qty) > int(line.qty_picked):
                raise osv.except_osv(
                         _('Warning!'),
                         _('There\'re still {0} remaining units of the product {1} - {2} '.format(line.qty - line.qty_picked, line.product.name.encode('utf-8'), line.ean13)))

        msg = {
            'type': 'notification',
            'author_id': self.pool.get('res.users').browse(cr, uid, uid).partner_id.id, #uid,
            'partner_ids': [(4, this.create_uid.partner_id.id)],
            #'notified_partner_ids': [(6,0,(order.supplier_id))],
            'body': _compose_body(this),
            'subject': 'Order {0} processed.'.format(this.reference),
        }
        self.pool.get('mail.message').create(cr, SUPERUSER_ID, msg, context) 
        this.write({'status': 'completed'})
        
        return self.pool['report'].get_action(cr, uid, ids, 'shopping_clubs.report_supplier_order_picking', context=context)
    
        """
        return {
                'type': 'ir.actions.act_window',
                'res_model': 'supplier.order.details',
                'view_mode': 'form',
                'res_id': ids[0]
            }
        """
        
    def action_missing_units(self, cr, uid, ids, context=None):
    
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        this = self.browse(cr, uid, ids)
        
        rows = [collections.OrderedDict({'Ean': line.product.ean13,
                                         'Product': line.product.name, 
                                         'Missing': line.qty - line.qty_picked,
                                         'Order': line.sod_id.reference}) for line in this.line_details]
        
        rows[:] = [row for row in rows if int(row.get('Missing')) > 0]
        
        if rows:
            cso = StringIO()
            csv_writer = csv.DictWriter(cso, rows[0].keys(), delimiter=';')
            csv_writer.writeheader()
            csv_writer.writerows(rows)
            
            _name = this.reference
            irattachment = self.pool.get('ir.attachment')
            ir_id = irattachment.search(cr, uid, [('res_model', '=', 'campaigns.clubs.rel'), ('res_id', '=', ids[0]), ('name', 'like', 'Missing_units_{0}.csv'.format(_name))])
            if ir_id:
                irattachment.write(cr, uid, ir_id, {'db_datas': cso.getvalue().encode('base64')})
            else:
                ir_id = irattachment.create(cr, uid, 
                                            {'name': 'Missing_units_{0}.csv'.format(_name),
                                             'datas_fname': 'Missing_units_{0}.csv'.format(_name),
                                             'db_datas': cso.getvalue().encode('base64'),
                                             'type': 'binary',
                                             'res_model': 'supplier.order.details',
                                             'res_id': ids[0]})
            
            return {
                    'name'     : 'Download',
                    'res_model': 'ir.actions.act_url',
                    'type'     : 'ir.actions.act_url',
                    'target'   : 'current',
                    'url'      : 'web/binary/saveas?model=ir.attachment&field=datas&filename_field=name&id={0}'.format(ir_id[0] if isinstance(ir_id, list) else ir_id)
            }
        
    def send_mail(self, cr, uid, ids, context=None):
        email_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'shopping_clubs', 'email_template_notify_completed_order')[1]
        self.pool.get('email.template').send_mail(cr, uid, email_id, uid, force_send=True, context=context)
      
        return True
    
    def get_pallet_id(self, cr, uid, ids, context=None):
        this = self.browse(cr, uid, ids)
        
        _new_pallet_id = '{0}{1}{2:02d}'.format('P',
                                            this.reference,
                                            len(this.pallets) + 1)
        
        return _new_pallet_id
    
        
    def print_details(self, cr, uid, ids, context=None):
        
        assert len(ids) == 1, 'This option only should be called with one id'
        return self.pool['report'].get_action(cr, uid, ids, 'shopping_clubs.repor_so_picking', context=context)
    
supplier_order_details()

class supplier_order_details_line(orm.Model):
    
    _name = 'supplier.order.details.line'
    
    def _get_product_ean(self, cr, uid, ids, field, args, context=None):
        res = {}
        
        for id in ids:
            res[id] = self.browse(cr, uid, id).product.ean13
        return res
    
    def _get_picked_qty(self, cr, uid, ids, field, args, context=None):
        res = {}
        package_ids = []
        for pid in ids:
            package_ids.extend([package.id for pallet in self.browse(cr, uid, pid).sod_id.pallets for package in pallet.packages])
         
        package_ids = list(set(package_ids))
        
        package_product = self.pool.get('supplier.order.package.product')
        for pid in ids:
            this = self.browse(cr, uid, pid)
            
            product_ids = package_product.search(cr, uid, [('product','=',this.product.id), ('package_id','in',package_ids)])
            res[pid] = sum([product.qty for product in package_product.browse(cr, uid, product_ids)])
            
        return res
    
    def _get_original(self, cr, uid, ids, field, args, context=None):
        
        return dict([(_id, True if len(self.browse(cr, uid, _id).product.product_originals) > 0 else False) for _id in ids]) 
    
    _columns = {
        'product': fields.many2one('product.product', 'Products', ondelete='cascade'),
        'ean13': fields.function(_get_product_ean, type="char", string="Product ean"),
        'qty': fields.integer('Quantity'),
        'qty_picked': fields.function(_get_picked_qty, type="integer", string='Alredy picked'),
        'special_info': fields.char('Info', size=30),
        'sod_id': fields.many2one('supplier.order.details', 'Supplier order details', ondelete='cascade'),
        'has_original': fields.function(_get_original, type="boolean", string='Has original')
    }

    _defaults = {
        'qty_picked': 0,
    }
    
    def action_download_original(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This action should only be used with one id at a time'
        this = self.browse(cr, uid, ids)
        res_id = self.pool.get('product.original').search(cr, uid, [('product_id','=',this.product.id)])            
        if res_id:
            if len(res_id) > 1:
                _logger.info(res_id)

                for _original in this.product.product_originals:
                    with open('/var/tmp/tozip/{0}'.format(_original.filename), 'w+') as img:
                        img.write(_original.image.decode('base64'))
        
                zippath = zipdir('{0}'.format(this.product.name.encode('utf-8')), '/var/tmp/tozip')
                with open(zippath, 'rb') as fff:
                    attachment_csv = fff.read().encode('base64')
                    #stream_csv = base64.b64encode(fff.read())
                
                try: 
                    os.remove(zippath)
                    shutil.rmtree('/var/tmp/tozip')
                except OSError:
                    pass
                
                irattachment = self.pool.get('ir.attachment')
                ir_id = irattachment.search(cr, uid, [('res_model', '=', 'supplier.order.details.line'), ('res_id', '=', ids[0]), ('name', 'like', '%.zip')])
                if ir_id:
                    irattachment.write(cr, uid, ir_id, {'db_datas': attachment_csv})
                else:
                    ir_id = self.pool.get('ir.attachment').create(cr, uid, 
                                                          {'name': zippath.replace('/var/tmp/', ''),
                                                           'datas_fname': zippath.replace('/var/tmp/', ''),
                                                           'db_datas': attachment_csv,
                                                           'type': 'binary',
                                                           'res_model': 'supplier.order.details.line',
                                                           'res_id': ids[0]})  
                return {
                        'name'     : 'Download',
                        'res_model': 'ir.actions.act_url',
                        'type'     : 'ir.actions.act_url',
                        'target'   : 'current',
                        'url'      : 'web/binary/saveas?model=ir.attachment&field=datas&filename_field=name&id={0}'.format(ir_id[0] if isinstance(ir_id, list) else ir_id)
                }
            else:
                return {
                        'name'     : 'Download',
                        'res_model': 'ir.actions.act_url',
                        'type'     : 'ir.actions.act_url',
                        'target'   : 'current',
                        'url'      : 'web/binary/saveas?model=product.original&field=image&filename_field=filename&id={0}'.format(res_id[0])
                }
        
    def action_download_label(self, cr, uid, ids, context=None):
        view_ref = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'shopping_clubs', 'labels_generator_wizard')
        view_id = view_ref and view_ref[1] or False,
        return {
            'type': 'ir.actions.act_window',
            'name': _('Generate labels'),
            'res_model': 'labels.generator',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'target': 'new',
            'context': {'product': self.browse(cr, uid, ids[0]).product.id}
        }
        
supplier_order_details_line()

class supplier_order_pallet(orm.Model):
    
    _name = 'supplier.order.pallet'
    
    def _get_supplier_name(self, cr, uid, ids, field_name, args, context=None):
        
        return dict([(_id, self.browse(cr, uid, _id).sod_id.supplier_id.name) for _id in ids])
        #return {ids[0]: self.browse(cr, uid, ids).sod_id.supplier_id}
    
    _columns = {
        'supplier': fields.function(_get_supplier_name, string='Supplier', type='char', relation='res.partner', store=True),
        'sod_id': fields.many2one('supplier.order.details', 'Picking', ondelete='cascade'),
        'order': fields.char('Order', size=10),
        'packages': fields.one2many('supplier.order.package', 'pallet_id', 'Packages'),
        'reference': fields.char('Pallet number', size=15),
        'supplier_id': fields.related('sod_id', 'supplier_id',
                                      type = 'many2one',
                                      relation = 'res.partner',
                                      string = 'Supplier id',
                                      store = True),
    }
    
    def print_package_label(self, cr, uid, pallet_id, context=None):
        return self.pool['report'].get_action(cr, uid, pallet_id, 'shopping_clubs.so_pallet_details', context=context)
    
    def write_pallet(self, cr, uid, sod_id, pallet_id, packages, context=None):        
        if not sod_id:
            return {'error': 'Supplier order details id not defined.'}
        if pallet_id:
            this = self.browse(cr, uid, pallet_id)
        else:
            ref = self.pool.get('supplier.order.details').browse(cr, uid, sod_id).get_pallet_id()
            _logger.info('ref -> {0}'.format(ref))
            this = self.browse(cr, uid, self.create(cr, uid, {'sod_id': sod_id, 'reference': ref}))
        """   
        if len(this.packages) > 0:
            for package in this.packages:
                this.write({'packages': [(2, package.id)]})
        _logger.info('Packages ar -> {0}'.format(packages))
        """
        this.write({'packages': [(6, 0, packages)]})
        
        return {'id': this.id, 'reference': this.reference}
        
    def get_package_id(self, cr, uid, ids, context=None):
        this = self.browse(cr, uid, ids)
        """
        _new_package_id = '{0}{1}{2}'.format('B',
                                            this.sod_id.reference,
                                            len(this.packages))
        """
        _new_package_id = '{0}{1:08d}{2}'.format('B',
                                            this.id,
                                            len(this.packages) + 1)
        return _new_package_id
        
supplier_order_pallet()

class supplier_order_package(orm.Model):

    _name = 'supplier.order.package'
    
    _columns = {
        'pallet_id': fields.many2one('supplier.order.pallet', 'Pallet', ondelete='cascade'),
        'products': fields.one2many('supplier.order.package.product', 'package_id', 'Products'),
        'reference': fields.char('Package number', size=15),
        'supplier': fields.related('sod_id',
                                   'supplier_id',
                                   type="many2one",
                                   relation="supplier.order.details",
                                   string="Supplier",
                                   store=False)
    }
    
    def print_package_label(self, cr, uid, package_id, context=None):
        return self.pool['report'].get_action(cr, uid, package_id, 'shopping_clubs.so_package_details', context=context)
    
    def write_package(self, cr, uid, pallet_id, package_id, products, context=None):
        """
        @param pallet_id: Id of the pallet.
        @param package_id: Id of the package. 
        @param products: A dict containing {product_id: xx,
                                            product_name: zxxx,
                                            qty: xxx,
                                            ean13: xxxxxxxxxxxx}
        """
     
        if not pallet_id:
            return {'error': 'Pallet id not defined.'}
        if package_id:
            this = self.browse(cr, uid, package_id)
        else:
            ref = self.pool.get('supplier.order.pallet').browse(cr, uid, pallet_id).get_package_id()
            this = self.browse(cr, uid, self.create(cr, uid, {'pallet_id': pallet_id, 'reference': ref}))
            
        if len(this.products) > 0:
            this.write({'products': [(2, product.id) for product in this.products]})
            
        for product in products:
            this.write({'products': [(0, 0, {'package_id': this.id,
                                             'product': product.get('product_id'), 
                                             'qty': product.get('qty')})]})
            

        return {'id': this.id}
supplier_order_package()

class supplier_order_package_product(orm.Model):
    
    _name = 'supplier.order.package.product'
    _rec_name = 'name'
    
    def _get_name(self, cr, uid, ids, field, args, context=None):
        try:
            return dict([(x, self.browse(cr, uid, x).product.name) for x in ids])
        except Exception as ex:
            _logger.info(ex)
            return False
        
        return False
    
    def _get_ean13(self, cr, uid, ids, field, args, context=None):
        try:
            return dict([(x, self.browse(cr, uid, x).product.ean13) for x in ids])
        except Exception as ex:
            _logger.info(ex)
            return False
        
        return False
        
    _columns = {
        'package_id': fields.many2one('supplier.order.package', 'Package', ondelete='cascade'),
        'product': fields.many2one('product.product', 'Product', ondelete='restrict'),
        'name': fields.function(_get_name, type="char", string='Product name'),
        'ean13': fields.function(_get_ean13, type="char", string='Ean13'),
        'qty': fields.integer('Quantity')
    }
supplier_order_package_product()

class supplier_warehouse_line(orm.Model):
    
    _name = 'supplier.warehouse.line'
    
    _columns = {
        'product': fields.many2one('product.product', 'Product'),
        'qty': fields.integer('Quantity'),
        'warehouse_id': fields.many2one('supplier.warehouse')
    }
    
supplier_warehouse_line()

class supplier_warehouse(orm.Model):

    _name = 'supplier.warehouse'
    
    def _get_warehouse_name(self, cr, uid, ids, field, arg, context=None):
        return dict([(_id, self.browse(cr, uid, _id).supplier.name) for _id in ids])
        
    _columns = {
        'name': fields.function(_get_warehouse_name, type="char", string="Name"),
        'supplier': fields.many2one('res.partner', 'Supplier', ondelete="cascade"),
        'warehouse_lines': fields.one2many('supplier.warehouse.line', 'warehouse_id', 'Lines')
    }

    def action_upload_stock_wizard(self, cr, uid, ids, context=None):
        view_ref = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'shopping_clubs', 'upload_stock_wizard')
        view_id = view_ref and view_ref[1] or False,
        return {
            'type': 'ir.actions.act_window',
            'name': _('Upload stock'),
            'res_model': 'upload.stock',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'target': 'new',
            'context': {'warehouse': ids[0]}
        }
        
supplier_warehouse()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
