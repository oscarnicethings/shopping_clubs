# -*- coding: utf-8 -*-
########################
# Created on 20/8/2014
#
# @author: Oscar Blanco
########################

import openerp
import logging

from openerp import netsvc, tools, pooler
from openerp.osv import fields,orm
from openerp.tools.translate import _

class category(orm.Model):
    '''
    Defines the category of a Shopping club, including a percentual discount applied in the on the selling campaign.
    '''
    _name = 'category'
    
    _columns = {
        'name': fields.char('Name', size=10, required=True),
        'discount': fields.integer('Discount amount')
    }
    
    _defaults = {
        'discount': 0
    }
    
    _sql_constraints = [
        ('unique_name', 'unique(name)', 'A category with the same name already exists')
    ]
    
category()    
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: