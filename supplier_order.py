# -*- coding: utf-8 -*-
########################
# Created on 30/9/2014
#
# @author: openerp
########################

import openerp
import logging

from openerp import netsvc, tools, pooler, api
from openerp.osv import fields, orm, osv
from openerp.tools.translate import _

from StringIO import StringIO
import csv
import base64
import os
import collections

from tools import convert

_logger = logging.getLogger(__name__)

class supplier_order(osv.osv_memory):
    '''
    Generate production orders foreach 
    '''
    
    _name = 'supplier.order'
    _rec_name = 'ccrel_id'
    
    _columns = {
        'lines': fields.one2many('supplier.order.line', 'so_id', 'Lines'),
        'ccrel_id': fields.many2one('campaigns.clubs.rel', 'Selling campaign'),
        'state': fields.selection([('init', 'init'), ('calculate', 'calculate'), ('finish', 'finish')]),
       # 'supplier_name': fields.related('so_id', 'name', type="one2many", relation="supplier.order.line", string="Supplier", store=False)      
    }

    _defaults = {
        'state': 'init'
    }
  
    def _write_cost_lines(self, cr, uid, ids, context=None):
        _lines = self.read(cr, uid, ids[0], ['lines'])
        _buy_route_id = self.pool.get('stock.location.route').search(cr, uid, [('name', '=', 'Buy')])
        ccrel_id = context.get('ccrel_id')
        _sod_obj = self.pool.get('supplier.order.details')
        _sol_obj = self.pool.get('supplier.order.line')
        _ccrel_obj = self.pool.get('campaigns.clubs.rel')
        _unlink_ids = _ccrel_obj.read(cr, uid, context.get('ccrel_id'), ['supplier_order_details'])['supplier_order_details']
        
        # Remove all previous lines (files)
        if _unlink_ids and len(_unlink_ids) > 0:
            _sod_obj.unlink(cr, uid, _unlink_ids)
        # If lines still there's something wrong :(
        if len(_lines.get('lines')) == 0:
            _p_obj = self.pool.get('product.product')
            _supp_obj = self.pool.get('res.partner')
            
            self.write(cr, uid, ids[0], {'ccrel_id': ccrel_id})
            #campaign_id = _ccrel_obj.read(cr, uid, ccrel_id, ['id_campaign'])['id_campaign'][0]
            #products = self.pool.get('campaign').read(cr, uid, campaign_id, ['campaign_products'])['campaign_products']
            products = [sl.product.id for sl in self.pool.get('campaigns.clubs.rel').browse(cr, uid, ccrel_id).sales_lines]
           
            _m_types = {}
            _m_types_supp = {}
            lines_ids = []
            _to_buy_prodcuts = []
            for p_id in products:
                _product = _p_obj.read(cr, uid, p_id, ['product_class', 'route_ids'])
                _p_class = _product.get('product_class')
                _p_route = _product.get('route_ids')
                if _p_route == _buy_route_id:
                    _to_buy_prodcuts.append(p_id)
                elif _p_class:
                    _p_class = _p_class[0]
                    _p_manufacturing = _p_obj.read(cr, uid, p_id, ['manufacturing'])['manufacturing']
                    ## Check which supplier can do that manufacturing type
                    for _m_id in _p_manufacturing:
                        _suppliers = _supp_obj.search(cr, uid, [('manufacturing_type', '=', _m_id)])
                        if not _m_types.has_key(_m_id):
                            _m_types[_m_id] = [_p_class]
                            _m_types_supp[_m_id] = _suppliers                        
                        else:
                            if _p_class not in _m_types.get(_m_id):
                                _m_types[_m_id].append(_p_class)
                            for _supplier in _suppliers:
                                if _supplier not in _m_types_supp.get(_m_id):
                                    _m_types_supp[_m_id].append(_supplier)
                   
            ## Prepare the lines to show on the wizard. The user can now choose which supplier
            ## is gonna do each manu type.
            for _m, _ss in _m_types_supp.items():
                lines_ids.append(_sol_obj.create(cr, uid, {'manufacturing_type': _m}))
            
            ## The products that have to be bought from a supplier
            if _to_buy_prodcuts and len(_to_buy_prodcuts) > 0:
                self._write_purchase_orders(cr, uid, ids[0], ccrel_id, _to_buy_prodcuts)
            if lines_ids and len(lines_ids) > 0:
                self.write(cr, uid, ids[0], {'lines': [(6, False, lines_ids)]})
            
            return lines_ids
                
    def _write_purchase_orders(self, cr, uid, ids, ccrel_id = None, products_to_buy = None):
        """
        Write csv files containing the products bought to a supplier.
        """
        _sl_obj = self.pool.get('sales.line')
        _ccrel_obj = self.pool.get('campaigns.clubs.rel')
        _p_obj = self.pool.get('product.product')
        _psupll_obj = self.pool.get('product.supplierinfo')
        _sup_obj = self.pool.get('res.partner')
        _orders = {}

        if products_to_buy is not None and ccrel_id is not None:
            for _p_id in products_to_buy:
                _product = _p_obj.read(cr, uid, _p_id, ['seller_ids', 'product_code', 'default_code', 'name', 'id'])
                try:
                    _supp_id = _psupll_obj.browse(cr, uid, _product.get('seller_ids')[0]).name.id
                except:
                    try:
                        _p_name =_product.get('name').encode('utf-8')
                    except:
                        _p_name =_product.get('name').decode('utf-8')
                    raise osv.except_osv(
                        _('Error!'),
                        _('The product {0} with product code {1} has no supplier assigned.'.format(_p_name, _product.get('product_code')))
                    )
                    
                _p_code = _product.get('product_code')
                _default_code = _product.get('default_code')
                _p_name = _product.get('name')
                _sl = _sl_obj.browse(cr, uid,
                                     _sl_obj.search(cr, uid, [('product', '=', _product.get('id')), ('ccrel_id', '=', ccrel_id)]))
                _qty = _sl.qty
                _special_info = _sl.special_info
                if int(_qty) > 0:    
                    if _supp_id not in _orders.keys():
                        _orders[_supp_id] = [{'product': _p_name, 'reference': _p_code, 
                                              'old reference': _default_code, 'quatity': _qty, 
                                              'special_info': _special_info, 'product_id': _product.get('id')}]
                    else:
                        _orders[_supp_id].append({'product': _p_name, 'reference': _p_code, 
                                                  'old reference': _default_code, 'quatity': _qty, 
                                                  'special_info': _special_info, 'product_id': _product.get('id')})
        
        if _orders:
            this = _ccrel_obj.browse(cr, uid, ccrel_id)
            if this.id_campaign.name:
                _f_name = this.id_campaign.name
            elif this.name:
                _f_name = this.name
            elif this.campaign_number:
                _f_name = this.campaign_number
            elif this.purchase_order:
                _f_name = this.purchase_order
            elif this.id_club:
                _f_name = this.id_club
                
            for _sup_id, _products in _orders.items():
                cso = StringIO()
                csv_writer = csv.DictWriter(cso, _products[0].keys(), delimiter='|')
                csv_writer.writeheader()
                csv_writer.writerows(convert(_products))
                
                _sodl = []
                for _p in _products:
                    _sodl.append((0, 0, {'product': _p.get('product_id'), 
                                         'qty': _p.get('quatity'),
                                         'special_info': _p.get('special_info')}))
                _ccrel_obj.write(cr, uid, ccrel_id,
                                 {'supplier_order_details': [(0, 0, {'file_name': 'Purchase_{0}_{1}'.format(_sup_obj.browse(cr, uid, _sup_id).name.encode('utf-8'), _f_name.replace(' ', '-').encode('utf-8')),
                                                                     'file_stream': base64.b64encode(cso.getvalue()),
                                                                     'supplier_id': _sup_id,
                                                                     'ccrel_id': ccrel_id,
                                                                     'line_details': _sodl,
                                                                     'order_type': 'purchase'})]})
        return True
    
    def _write_manufacturing_orders(self, cr, uid, ids, sol_lines_ids=None, context=None):

        _ccrel_id = context.get('ccrel_id')
        _ccrel_obj = self.pool.get('campaigns.clubs.rel')
        _sl_obj = self.pool.get('sales.line')
        _pp_obj = self.pool.get('product.product')
        _sol_obj = self.pool.get('supplier.order.line')
        
        _sol_lines = _sol_obj.read(cr, uid, sol_lines_ids, ['supplier_id', 'manufacturing_type'])
        _manu_sup_assoc = {}  # Holds the association of suppliers and manufacturing process
        for _sol_line in _sol_lines:
            _manu_sup_assoc[_sol_line.get('manufacturing_type')[0]] = _sol_line.get('supplier_id')
        
        _buy_route_id = self.pool.get('stock.location.route').search(cr, uid, [('name', '=', 'Buy')])
        _sales_lines = (_ccrel_obj.read(cr, uid, _ccrel_id, ['sales_lines'])).get('sales_lines')
        _products = _sl_obj.read(cr, uid, _sales_lines, ['product', 'qty', 'special_info'])
        _supp_products = {}
        for _p in _products:
            _p_info, _qty, _special_info = map(_p.get, ('product', 'qty', 'special_info'))
            _p_obj = _pp_obj.read(cr, uid, _p_info[0], ['manufacturing', 'name', 'product_code', 'default_code', 'product_class', 'route_ids'])
            
            if int(_qty) == 0:
                continue
            if _p_obj.get('route_ids') == _buy_route_id:
                continue
            for _p_m in _p_obj.get('manufacturing'):
                if _manu_sup_assoc.get(_p_m) not in _supp_products:
                    _supp_products[_manu_sup_assoc.get(_p_m)] = [{'qty': _qty,
                                                                  'special_info': _special_info,
                                                                  'product_id': _p_info[0],
                                                                  'product_name':_p_obj.get('name'),
                                                                  'product_code': _p_obj.get('product_code'),
                                                                  'default_code': _p_obj.get('default_code'),
                                                                  'product_class': _p_obj.get('product_class')[1]}]
                else:
                    _supp_products[_manu_sup_assoc.get(_p_m)].append({'qty': _qty,
                                                                      'special_info': _special_info,
                                                                      'product_id': _p_info[0],
                                                                      'product_name':_p_obj.get('name'),
                                                                      'product_code': _p_obj.get('product_code'),
                                                                      'default_code': _p_obj.get('default_code'),
                                                                      'product_class': _p_obj.get('product_class')[1]})

        if self._write_supplier_order_csv(cr, uid, ids, _supp_products, _manu_sup_assoc, context):
            _logger.info('CSV creation OK')
            
        return         
    
    def _write_supplier_order_csv(self, cr, uid, ids, data=None, manufacturing_types=None, context=None):
        
        if data is None:
            return False
        
        _sod_obj = self.pool.get('supplier.order.details')
        _sodl_obj = self.pool.get('supplier.order.details.line')
        _ccrel_obj = self.pool.get('campaigns.clubs.rel')
        
        this = _ccrel_obj.browse(cr, uid, context.get('ccrel_id'))
        if this.id_campaign:
            _campaign_name = this.id_campaign.name.replace(' ', '-')
        else:
            _campaign_name = this.name
        try:
            _campaign_name = _campaign_name.encode('utf-8')
        except:
            pass
        _sod_lines = []
        for _sup_key, _sup_data in data.items():
            
            cso = StringIO()
            _sup_id, _sup_name = _sup_key
            csv_writer = csv.DictWriter(cso, _sup_data[0].keys(), delimiter='|')
            csv_writer.writeheader()
            csv_writer.writerows(convert(_sup_data))
            
            _sodl = []
            for _line in _sup_data:
                _sodl.append((0, 0, {'product': _line.get('product_id'), 
                                     'qty': _line.get('qty'),
                                     'special_info': _line.get('special_info')}))
            
            manu = {}
            if manufacturing_types is not None:
                for m_type, supplier in manufacturing_types.items():
                    if supplier[0] == _sup_id:
                        if _sup_id in manu.keys():
                            manu[_sup_id].append(m_type)
                        else:
                            manu[_sup_id] = [m_type]
                            
            _ccrel_obj.write(cr, uid, context.get('ccrel_id'), 
                         {'supplier_order_details': [(0, 0, {'file_name': 'Manufacturing_{0}_{1}'.format(_sup_name.encode('utf-8'), _campaign_name),
                                                             'file_stream': base64.b64encode(cso.getvalue()),
                                                             'supplier_id': _sup_id,
                                                             'ccrel_id': context.get('ccrel_id'),
                                                             'line_details': _sodl,
                                                             'manufacturing': [(6, 0, manu.get(_sup_id))],
                                                             'order_type': 'manufacturing'})]})
       
        return True

    def action_next(self, cr, uid, ids, context=None):
        _sol_lines = self._write_cost_lines(cr, uid, ids, context)
        self.write(cr, uid, ids[0], {'state': 'calculate', }, context=context)
        context['ccrel_id'] = context.get('ccrel_id')
        context['sol_lines'] = _sol_lines
        # return view
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'supplier.order',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': ids[0],
            'views': [(False, 'form')],
            'target': 'new',
            'context': context
        }
  
    def action_calculate(self, cr, uid, ids, context=None):
        self._write_manufacturing_orders(cr, uid, ids, context.get('sol_lines'), context)
        self.write(cr, uid, ids[0], {'state': 'finish', }, context=context)
        
        return self.action_close(cr, uid, ids, context)
        
    def action_close(self, cr, uid, ids, context=None):
        return {
            'type': 'ir.actions.client',
            'tag': 'reload'
        }
        
supplier_order()

class supplier_order_line(osv.osv_memory):
    
    _name = 'supplier.order.line'
    
    _rec_name = 'supplier_id'
    
    _columns = {
        'manufacturing_type': fields.many2one('manufacturing.type', 'Manufacturing type'),
        'supplier_id': fields.many2one('res.partner', 'Supplier', domain="[('manufacturing_type', '!=', False)]"),
        'so_id': fields.many2one('supplier.order', 'Sorder', ondelete='cascade'),
        'cost': fields.float('Cost', digits=(10, 2)),
    }
    
    _defaults = {
        'cost': 0.0
    }

    def has_manufacturing_type(self, cr, uid, ids, supplier_id, manufacturing_type, context=None):
        
        _supplier_manu = self.pool.get('res.partner').read(cr, uid, supplier_id, ['manufacturing_type'])['manufacturing_type']
        if manufacturing_type not in _supplier_manu or not _supplier_manu:         
            return {'warning': {
                        'title': _('Warning!'),
                        'message' : _('This supplier does not have associated that manufacturing type.\nPlease, choose another supplier.')
                    },
                    'value': {
                        'cost': 0.0,
                        'supplier_id': False
                    }
            }
        else:
            _so_id = self.read(cr, uid, ids[0], ['so_id'])['so_id'][0]
            ccrel_id = self.pool.get('supplier.order').read(cr, uid, _so_id, ['ccrel_id'])['ccrel_id'][0]
            _cost = self._get_supplier_cost(cr, uid, supplier_id, manufacturing_type, ccrel_id, context)
    
            self.write(cr, uid, ids[0], {'cost': _cost})
            return {'value': {'cost': _cost}}
                
    def _get_supplier_cost(self, cr, uid, supplier_id, manufacturing_type, ccrel_id, context=None):
        _supp_obj = self.pool.get('res.partner')
        _ccrel_obj = self.pool.get('campaigns.clubs.rel')
        _pp_obj = self.pool.get('product.product')
        _smc_obj = self.pool.get('supplier.manufacturing.cost')
        
        _products = _ccrel_obj.browse(cr, uid, ccrel_id).id_campaign.campaign_products.ids
        
        _p_classes = _pp_obj.read(cr, uid, _products, ['product_class'])
        _pc_ids = []
        for _p_class in _p_classes:
          
            _pc_id = _p_class.get('product_class')[0] if _p_class.get('product_class') else False 
            if _pc_id and _pc_id not in _pc_ids:
                _pc_ids.append(_pc_id)
      
        _sales_lines = _ccrel_obj.browse(cr, uid, ccrel_id).sales_lines.ids
        _pp_detail = self.pool.get('sales.line').read(cr, uid, _sales_lines, ['product', 'qty'])
    
        _cost_m = 0.0
        for _pc_id in _pc_ids:
            _query = [
                ('supplier_id', '=', supplier_id),
                ('manufacturing_type_id', '=', manufacturing_type),
                ('product_class_id', '=', _pc_id)
            ]
            
            _ii = _smc_obj.search(cr, uid, _query)
       
            _supplier_costs = _smc_obj.read(cr, uid, _ii, ['manufacturing_cost'])
            if _supplier_costs:
                _supplier_costs = _supplier_costs[0]['manufacturing_cost']
                
            for _p_detail in _pp_detail:
                if _p_detail.get('product')[0] in _products and _p_detail.get('qty') > 0:
               
                    if _pp_obj.read(cr, uid, _p_detail.get('product')[0], ['product_class'])['product_class'][0] == _pc_id:
                        _cost_m += _p_detail.get('qty') * _supplier_costs if _supplier_costs else 0
                      
        return _cost_m
    
    def update_fields(self, cr, uid, ids, supplier_id, manufacturing_type, context=None):

        _so_id = self.read(cr, uid, ids[0], ['so_id'])['so_id'][0]
        ccrel_id = self.pool.get('supplier.order').read(cr, uid, _so_id, ['ccrel_id'])['ccrel_id'][0]
        _cost = self._get_supplier_cost(cr, uid, supplier_id, manufacturing_type, ccrel_id, context)

        self.write(cr, uid, ids[0], {'cost': _cost})
        res = {}
        res['cost'] = _cost
        return res
   
supplier_order_line()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
