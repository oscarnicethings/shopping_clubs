# -*- coding: utf-8 -*-
########################
# Created on 16/9/2014
#
# @author: openerp
########################

import openerp
import logging

from openerp import netsvc, tools, pooler
from openerp.osv import fields,orm
from openerp.tools.translate import _

_logger = logging.getLogger(__name__)

class stock_move(orm.Model):
    
    _inherit = 'stock.move'
    _columns = {'product_code': fields.char('Product reference', select=True)}
    _defaults = {'product_code': 'NAN'}
    
    def create(self, cr, uid, vals, context=None):
        if vals.get('product_id'):
            prod_obj = self.pool.get('product.product')
            vals['product_code'] = prod_obj.browse(cr, uid, vals['product_id'], context=context).product_code
        return super(stock_move, self).create(cr, uid, vals, context=context)
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: