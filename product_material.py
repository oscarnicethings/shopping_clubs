# -*- coding: utf-8 -*-
########################
# Created on 30/9/2014
#
# @author: openerp
########################

import openerp
import logging

from openerp import netsvc, tools, pooler
from openerp.osv import fields, orm
from openerp.tools.translate import _

import openerp.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class product_material(orm.Model):
    '''
    classdocs
    '''
    _inherits = {'product.template': 'product_tmpl_id'}
    _inherit = 'mail.thread'
    _name = 'product.material'
    
    _defaults = {
        'sale_ok': False
    }
    
    _columns = {
        'product_tmpl_id': fields.many2one('product.template', 'Product Template', required=True, ondelete="cascade", select=True),
    }
    
    def onchange_uom(self, cursor, user, ids, uom_id, uom_po_id):
        if uom_id and uom_po_id:
            uom_obj = self.pool.get('product.uom')
            uom = uom_obj.browse(cursor, user, [uom_id])[0]
            uom_po = uom_obj.browse(cursor, user, [uom_po_id])[0]
            if uom.category_id.id != uom_po.category_id.id:
                return {'value': {'uom_po_id': uom_id}}
        return False

class product_material_list_line(orm.Model):
    """
    classdocs T_T
    """
    _name = 'product.material.list.line'
    
    def _uom_id_get_fnc(self, cr, uid, ids, name, args, context=None):       
        _uom_name = self.browse(cr, uid, ids[0]).product_id.uom_id.name      
        return {name: _uom_name}
        
    _columns = {
        'product_id': fields.many2one('product.product', 'Product'),
        'qty': fields.float('Units', digits_compute=dp.get_precision('Product UoS'),
            help='Units of product material'),
        'product_material_id': fields.many2one('product.material', 'Material'),
        'uom_id': fields.related('product_material_id',
                                 'uom_id',
                                 type="many2one",
                                 relation="product.uom",
                                 string="Unit of measure",
                                 store=False)
    }
    
product_material_list_line()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
