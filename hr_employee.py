# -*- coding: utf-8 -*-
########################
# Created on Mar 31, 2015
#
# @author: openerp
########################

import openerp
import logging

from openerp import netsvc, tools, pooler
from openerp.osv import fields,orm
from openerp.tools.translate import _

class sp_employee(orm.Model):
    '''
    classdocs
    '''
    
    _name = 'hr.employee'
    _inherit = 'hr.employee'
    
    def _get_partner_image(self, cr, uid, ids, context=None):
        
        return dict([(_id, self.browse(cr, uid, _id).user_id.image) for _id in ids])
        
    _columns = {
        'image': fields.function(_get_partner_image, type="binary", store=True, string="Image")
    }
    
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: