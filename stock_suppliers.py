# -*- coding: utf-8 -*-
########################
# Created on 26/9/2014
#
# @author: openerp
########################

import openerp
import logging

from openerp import netsvc, tools, pooler
from openerp.osv import fields,orm
from openerp.tools.translate import _

class stock_suppliers(orm.Model):
    '''
    classdocs
    '''
    
    _name = 'stock.suppliers'
    
    _columns = {
        'product': fields.many2one('product.product', 'Product'),
        'qty': fields.integer('Quantity'),
        'supplier': fields.many2one('res.partner', 'Supplier')
    }
    
stock_suppliers()
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: