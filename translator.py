# -*- coding: utf-8 -*-
########################
# Created on 4/9/2014
#
# @author: openerp
########################
from apiclient.discovery import build

class Translator(object):
    '''
    classdocs
    '''
    _api_key = 'AIzaSyB5mo9CeulwCa6ZlItrLjOetpBhjayBIKE'
    _action = 'translate'
    _version = 'v2'
    _service = None
    
    def __init__(self):
        self._service = build(self._action, 
                         self._version,
                         developerKey= self._api_key)
    
    def translate(self, string):
        
        tt = self._service.translations().list(source='es', target='en', q=[string.decode('utf-8')]).execute()
        return tt['translations'][0]['translatedText']
