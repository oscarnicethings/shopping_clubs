# -*- coding: utf-8 -*-
########################
# Created on 25/8/2014
#
# @author: openerp
########################

from __future__ import division
import openerp
import logging

from openerp import SUPERUSER_ID
from openerp import netsvc, tools, pooler
from openerp.osv import fields, orm
from openerp.tools.translate import _

import openerp.addons.decimal_precision as dp

from import_products import _pcode_placeholder
from openerp.addons.product.product import check_ean
from translator import Translator
from tools import sizeof_fmt
import base64
import os
import PIL
from PIL import Image
from base64 import encode
from email.encoders import encode_base64

_logger = logging.getLogger(__name__)

class product_special_price(orm.Model):
    
    _name = 'product.special.price'
    
    _columns = {
        'product': fields.many2one('product.product', 'Product', ondelete='cascade'),
        'club': fields.many2one('res.partner', 'Shopping club', domain = [('is_shopping_club','=',True)]),
        'price': fields.float(string='Price', digits_compute=dp.get_precision('Product Price')),
    }

class product_class(orm.Model):
    
    _name = 'product.class'
    
    _columns = {
        'name': fields.char('Name', size=100, required=True)
    }
    
    _sql_constraints = [
        ('product_class_name_uniq', 'unique(name)', 'The name of must be unique!'),
    ]
 
    def _get_default_class(self, cr, uid, ids=None, context=None):
        if not self.search(cr, uid, [('name', '=', 'Default')]):
            self.create(cr, uid, {'name': 'Default'})
        else:
            return self.search(cr, uid, [('name', '=', 'Default')])[0]
    
product_class()

class product_tag(orm.Model):

    """
    Classdocs uee
    """
    
    _name = 'product.tag'
    
    _columns = {
        'name': fields.char('Name', size=20, required=True)
    }
    
product_tag()

class product_color(orm.Model):

    """
    Classdocs ueeueueueuueueuueue
    """
    
    _name = 'product.color'
    
    _columns = {
        'name': fields.char('Name', size=20, required=True)
    }
    
product_tag()

class product_image(orm.Model):
    
    """
    Used to assign various images to the same product.
    """
    _name = 'product.image'
    _columns = {
        'product_id': fields.many2one('product.product', 'Product', ondelete='cascade'),
        'image': fields.binary('Image'),
        'filename': fields.char('Filename'),
    }
    
    def action_download(self, cr, uid, ids, context=None):
        
        assert len(ids) == 1, 'This option should only be used with one id at time.'
        
        return {
            'name'     : 'Download',
            'res_model': 'ir.actions.act_url',
            'type'     : 'ir.actions.act_url',
            'target'   : 'current',
            'url'      : 'web/binary/saveas?model=product.image&field=image&filename_field=filename&id={0}'.format(ids[0])
        }
        
    def create(self, cr, uid, vals, context=None):
        _p_id = vals.get('product_id')
        product = self.pool.get('product.product').browse(cr, uid, _p_id)
        vals['filename'] = '{0}_{1}.jpg'.format(product.product_code, len(product.product_images) + 1)        
        
        img_size = sizeof_fmt(((len(vals.get('image')) * 3) / 4 ))
        base_width = 2200
        if float(img_size.split(' ')[0]) > 2.0 and img_size.endswith('MB'):
            with open('/var/tmp/{0}'.format(vals.get('filename')), 'w+') as image:
                image.write(vals.get('image').decode('base64'))

            image = Image.open('/var/tmp/{0}'.format(vals.get('filename')))
            wpercent = (base_width / float(image.size[0]))

            hsize = int((float(image.size[1]) * float(wpercent)))
            image = image.resize((base_width, hsize), PIL.Image.ANTIALIAS)

            image.save('/var/tmp/{0}'.format(vals.get('filename')))
            vals['image'] = open('/var/tmp/{0}'.format(vals.get('filename')), 'rb').read().encode('base64')
            
            os.remove('/var/tmp/{0}'.format(vals.get('filename')))
        record_id = super(product_image, self).create(cr, uid, vals, context=context)                

        return record_id
    
product_image()

class product_original(orm.Model):
    
    """
    Used to assign various images to the same product.
    """

    _name = 'product.original'
    _columns = {
        'product_id': fields.many2one('product.product', 'Product', ondelete='cascade'),
        'image': fields.binary('Image'),
        'filename': fields.char('Filename'),
    }
    
    def create(self, cr, uid, vals, context=None):
        _p_id = vals.get('product_id')
        product = self.pool.get('product.product').browse(cr, uid, _p_id)
        vals['filename'] = '{0}_{1}.jpg'.format(product.product_code, len(product.product_originals) + 1)          
        record_id = super(product_original, self).create(cr, uid, vals, context=context)                

        return record_id
    
    def action_download(self, cr, uid, ids, context=None):
        
        assert len(ids) == 1, 'This option should only be used with one id at time.'
        
        return {
            'name'     : 'Download',
            'res_model': 'ir.actions.act_url',
            'type'     : 'ir.actions.act_url',
            'target'   : 'current',
            'url'      : 'web/binary/saveas?model=product.original&field=image&filename_field=filename&id={0}'.format(ids[0])
        }
        
product_original()

class product(orm.Model):
    '''
    Extends product.product class
    '''
    
    _inherit = 'product.product'
    
    def _get_max_units_sold(self, cr, uid, ids, field, args, context=None):
        res = {}
        _club_ids = self.pool.get('res.partner').search(cr, uid, [('shopping_club_category', 'in', [1, 2])])
        _ccrel_ids = self.pool.get('campaigns.clubs.rel').search(cr, uid, [('state', '=', 'finished'),
                                                                           ('id_club', 'in', _club_ids),
                                                                           ('total_units', '!=', 0)])
        _sl_obj = self.pool.get('sales.line')
        for _id in ids:
            sales_line_ids = _sl_obj.search(cr, uid, [('product', '=', _id), ('ccrel_id', 'in', _ccrel_ids)])
            sales_lines = _sl_obj.browse(cr, uid, sales_line_ids)
            res[_id] = max([sale_line.qty for sale_line in sales_lines]) if len(sales_lines) > 0 else 0
            
        return res
    
    def _get_min_units_sold(self, cr, uid, ids, field, args, context=None):
        res = {}
        _club_ids = self.pool.get('res.partner').search(cr, uid, [('shopping_club_category', 'in', [1, 2])])
        _ccrel_ids = self.pool.get('campaigns.clubs.rel').search(cr, uid, [('state', '=', 'finished'),
                                                                           ('id_club', 'in', _club_ids),
                                                                           ('total_units', '!=', 0)])
        _sl_obj = self.pool.get('sales.line')
        for _id in ids:
            sales_line_ids = _sl_obj.search(cr, uid, [('product', '=', _id), ('ccrel_id', 'in', _ccrel_ids)])
            sales_lines = _sl_obj.browse(cr, uid, sales_line_ids)
            res[_id] = min([sale_line.qty for sale_line in sales_lines]) if len(sales_lines) > 0 else 0
    
        return res
    
    def _get_max_units_sold_campaign(self, cr, uid, ids, field, args, context=None):
        res = {}
        _club_ids = self.pool.get('res.partner').search(cr, uid, [('shopping_club_category', 'in', [1, 2])])
        _ccrel_ids = self.pool.get('campaigns.clubs.rel').search(cr, uid, [('state', '=', 'finished'),
                                                                           ('id_club', 'in', _club_ids),
                                                                           ('total_units', '!=', 0)])
        _sl_obj = self.pool.get('sales.line')
        for _id in ids:
            this = self.browse(cr, uid, _id)
            sales_line_ids = _sl_obj.search(cr, uid, [('product', '=', this.id), ('ccrel_id', 'in', _ccrel_ids)])
            sales_lines = _sl_obj.browse(cr, uid, sales_line_ids)
            if len(sales_lines) > 0:
                for sales_line in sales_lines:
                    if sales_line.qty == this.max_units_sold:
                        res[_id] = sales_line.ccrel_id.id
            else:
                res[_id] = False
                
        return res
    
    def _get_min_units_sold_campaign(self, cr, uid, ids, field, args, context=None):
        res = {}
        _club_ids = self.pool.get('res.partner').search(cr, uid, [('shopping_club_category', 'in', [1, 2])])
        _ccrel_ids = self.pool.get('campaigns.clubs.rel').search(cr, uid, [('state', '=', 'finished'),
                                                                           ('id_club', 'in', _club_ids),
                                                                           ('total_units', '!=', 0)])
        _sl_obj = self.pool.get('sales.line')
        for _id in ids:
            this = self.browse(cr, uid, _id)
            sales_line_ids = _sl_obj.search(cr, uid, [('product', '=', this.id), ('ccrel_id', 'in', _ccrel_ids)])
            sales_lines = _sl_obj.browse(cr, uid, sales_line_ids)
            if len(sales_lines) > 0:
                for sales_line in sales_lines:
                    if sales_line.qty == this.min_units_sold:
                        res[_id] = sales_line.ccrel_id.id
            else:
                res[_id] = False
        
        return res
    
    def _get_ratio(self, cr, uid, ids, field, args, context=None):
        res = {}
        _club_ids = self.pool.get('res.partner').search(cr, uid, [('shopping_club_category', 'in', [1, 2])])
        _ccrel_ids = self.pool.get('campaigns.clubs.rel').search(cr, uid, [('state', '=', 'finished'),
                                                                           ('id_club', 'in', _club_ids),
                                                                           ('total_units', '!=', 0)])
        _sl_obj = self.pool.get('sales.line')
        for _id in ids:
            this = self.browse(cr, uid, _id)
            sales_line_ids = _sl_obj.search(cr, uid, [('product', '=', this.id), ('ccrel_id', 'in', _ccrel_ids)])
            sales_lines = _sl_obj.browse(cr, uid, sales_line_ids)
            res[_id] = sum([sales_line.qty for sales_line in sales_lines]) / len(sales_lines) if len(sales_lines) > 0 else 0
            
        return res
    
    _columns = {
        'is_multicampaign': fields.boolean('Is multicampaign', help='This product can belong to more than one campaign'),
        'is_on_campaign': fields.boolean('Is on campaign', help='This product is already on a campaign'),
        'is_web': fields.boolean('Only on web', help='This product only can be sold on web store'),
        'is_web': fields.boolean('Only on web', help='This product only can be sold on web store'),
        'is_development': fields.boolean('On development', help='This product is in development'),
        'agrupation_code': fields.char('Agrupation code', size=10),
        'description_en': fields.text('Description english', translate=True,
                    help="A precise description of the Product, used only for internal information purposes."),
        'campaign_price': fields.float('Campaign Price', digits_compute=dp.get_precision('Product Price'), help="Price offered to shopping clubs."),
        'name_en': fields.char('Name (english)'),
        'img_path': fields.char('Path', size=48),
        'product_code': fields.char('Product code', size=7),
        'manufacturing': fields.many2many('manufacturing.type', 'product_manufacturing_rel', 'manufacturing_id', 'product_id', 'Manufacturing'),
        'product_class': fields.many2one('product.class', 'Product class', required=True),
        'product_material_lines': fields.one2many('product.material.list.line', 'product_id', 'Materials list', ondelete='cascade'),
        'product_tags': fields.many2many('product.tag', 'product_product_tag_rel', 'product_id', 'product_tag_id', 'Tags'),
        'product_color': fields.many2one('product.color', 'Color'),
        'product_images': fields.one2many('product.image', 'product_id', 'Images'),
        'product_originals': fields.one2many('product.original', 'product_id', 'Originals'),
        'brand': fields.many2one('brand.brand', 'Brand'),
        'special_prices': fields.one2many('product.special.price', 'product', 'Special prices'),
        'max_units_sold': fields.function(_get_max_units_sold, type='integer', string='Max. units sold'),
        'max_units_sold_campaign': fields.function(_get_max_units_sold_campaign, type='many2one', relation='campaigns.clubs.rel', string='Max. units sold campaign'),
        'min_units_sold': fields.function(_get_min_units_sold, type='integer', string='Min. units sold'),
        'min_units_sold_campaign': fields.function(_get_min_units_sold_campaign, type='many2one', relation='campaigns.clubs.rel', string='Min. units sold campaign'),
        'ratio': fields.function(_get_ratio, type='float', string='Ratio')
    }

    _defaults = {'product_class': lambda self, cr, uid, ids, context = None:  self.pool.get('product.class')._get_default_class(cr, uid)}
    
    def check_best_sellers(self, cr, uid, ids=None, context=None):
        
        p_ids = self.browse(cr, uid, [('ratio', '>', '0')])
        _logger.info(p_ids)
        return
    
    def name_get(self, cr, user, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        if not len(ids):
            return []

        def _name_get(d):
            name = d.get('name', '')
            code = d.get('default_code', False)
            if code:
                name = '%s' % (name)
            return (d['id'], name)

        partner_id = context.get('partner_id', False)

        # all user don't have access to seller and partner
        # check access and use superuser
        self.check_access_rights(cr, user, "read")
        self.check_access_rule(cr, user, ids, "read", context=context)

        result = []
        for product in self.browse(cr, SUPERUSER_ID, ids, context=context):
            variant = ", ".join([v.name for v in product.attribute_value_ids])
            name = variant and "%s (%s)" % (product.name, variant) or product.name
            sellers = []
            if partner_id:
                sellers = filter(lambda x: x.name.id == partner_id, product.seller_ids)
            if sellers:
                for s in sellers:
                    mydict = {
                              'id': product.id,
                              'name': s.product_name or name,
                              'default_code': s.product_code or product.default_code,
                              }
                    result.append(_name_get(mydict))
            else:
                mydict = {
                          'id': product.id,
                          'name': name,
                          'default_code': product.default_code,
                          }
                result.append(_name_get(mydict))

        return result
    
    def calculate_checksum(self, ean):
        """Calculates the checksum for EAN13-Code.
    
        :returns: The checksum for `ean`.
        :rtype: Integer
        """
        sum_ = lambda x, y: int(x) + int(y)
        evensum = reduce(sum_, ean[::2])
        oddsum = reduce(sum_, ean[1::2])
        return (10 - ((evensum + oddsum * 3) % 10)) % 10

    def get_product_code(self, cr, uid, ids, context=None):
        _self_code = self.read(cr, uid, ids[0], [])
        if _self_code:
            return str(_self_code)[1:-1]
        
        return False
    
    def assing_product_code(self, cr, uid, product_id, context = None):
        _code = _pcode_placeholder.format(int(self.browse(cr, uid, product_id).categ_id.id), product_id)
        self.write(cr, uid, product_id, {'product_code': _code})
        return _code
        
    def assign_ean13(self, cr, uid, product_id = None, product_code = None, context = None):
        if product_code is not None and product_id is not None:
            _manu_code = self.pool.get('custom.config.settings').get_default_manufacturer_code(cr, uid, {})
            if _manu_code is False:
                return False
            _manu_code = _manu_code.get('manufacturer_code')
            _p_code = product_code[2:]
            if _p_code and len(_p_code) == 5 and _manu_code:
                preapre_num = '{0}{1}'.format(_manu_code, _p_code)
                ean13 = '{0}{1}'.format(preapre_num, self.calculate_checksum(str(preapre_num)))
                self.write(cr, uid, product_id, {'ean13': ean13})
        return False
    
    def action_translate_description(self, cr, uid, ids, context = None):
        if self.browse(cr, uid, ids).description:
            self._translator = Translator()
            self.write(cr, uid, ids, {'description_en': self._translator.translate(self.browse(cr, uid, ids).description.encode('utf-8'))})
    
    def action_regenerate_ean(self, cr, uid, ids, context = None):
        product = self.browse(cr, uid, ids)
        if product.product_code:
            product.assign_ean13(product.id, product.product_code)
    
    def action_regenerate_product_code(self, cr, uid, ids, context = None):
        product = self.browse(cr, uid, ids)
        product.assing_product_code(product.id)
        product.assign_ean13(product.id, product.product_code)
    
    def action_labels_wizard(self, cr, uid, ids, context=None):
        
        view_ref = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'shopping_clubs', 'labels_generator_wizard')
        view_id = view_ref and view_ref[1] or False,
        return {
            'type': 'ir.actions.act_window',
            'name': _('Generate labels'),
            'res_model': 'labels.generator',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'target': 'new',
            'context': {'product': ids[0]}
        }
        
    def _write_smc_lines(self, cr, uid, ids, vals, context=None):
        try:
            if ('manufacturing' in vals.keys() and len(vals.get('manufacturing')) > 0):
                _supplier_obj = self.pool.get('res.partner')
                _smc_obj = self.pool.get('supplier.manufacturing.cost')
                
                _mt_ids = vals.get('manufacturing')[0][2]
                if 'product_class' in vals.keys() and vals.get('product_class'):
                    _prod_class_id = vals.get('product_class')
                else:
                    _prod_class_id = self.browse(cr, uid, ids[0]).product_class.id
                    
                for _mt_id in _mt_ids:
                    _supplier_ids = _supplier_obj.search(cr, uid, [('manufacturing_type', '=', _mt_id)])
                    for _sp_id in _supplier_ids:
                        _query = [('supplier_id', '=', _sp_id),
                                 ('manufacturing_type_id', '=', _mt_id),
                                 ('product_class_id', '=', vals.get('product_class'))]

                        if not len(_smc_obj.search(cr, uid, _query)) > 0:
                            _smc_obj.create(cr, uid,
                                            {'supplier_id': _sp_id,
                                             'manufacturing_type_id': _mt_id,
                                             'product_class_id': _prod_class_id,
                                             'manufacturing_cost': 0.0})
                return True
        except Exception as ex:
            _logger.error('Error when creating supplier manufacturing cost lines: {0}'.format(ex))
            return False
        
    # # On create, get product's manufacturing types and generate a supplier.manufacturing.cost entry for each supplier
    # # that has associated those manufacturing types.
    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        ctx = dict(context or {}, create_product_product=True)
        _p_id = super(product, self).create(cr, uid, vals, context=ctx)
        this = self.browse(cr, uid, _p_id)
        _logger.info(vals)
        if vals:
            self._write_smc_lines(cr, uid, _p_id, vals, context)
            if 'description' in vals and vals.get('description') and not vals.get('description_en'):
                self._translator = Translator()
                desc = vals.get('description')
                try:
                    desc = desc.encode('utf-8')
                except:
                    pass
                self.write(cr, uid, _p_id, {'description_en': self._translator.translate(desc)})

            if vals is not False and 'categ_id' in vals:
                self.write(cr, uid, _p_id, {'product_code': _pcode_placeholder.format(vals.get('categ_id'), _p_id)})
                self.assign_ean13(cr, uid, _p_id, _pcode_placeholder.format(vals.get('categ_id'), _p_id), context)
            
            if 'default_code' not in vals or vals.get('default_code') is False:
                this.write({'default_code': this.product_code})
                
        return _p_id
    
    ## On write, check if any manufacturing type has been added, if so, create a supplier.manufacturing.cost entry has on create method
    def write(self, cr, uid, ids, vals, context=None):
        self._write_smc_lines(cr, uid, ids, vals, context)
        return super(product, self).write(cr, uid, ids, vals, context=context)

    def copy(self, cr, uid, p_id, default=None, context=None):
        if context is None:
            context={}
        
        product_obj = self.browse(cr, uid, p_id, context)
        return super(product, self).copy(cr, uid, p_id, default=default, context=context)
    
product()

class template(orm.Model):
    
    _inherit = 'product.template'
    
    _columns = {
        'packing_width': fields.float('Packing width', digits=(10, 2)),
        'packing_height': fields.float('Packing height', digits=(10, 2)),
        'packing_length': fields.float('Packing length', digits=(10, 2)),
        'width': fields.float('Width', digits=(10, 2)),
        'height': fields.float('Height', digits=(10, 2)),
        'length': fields.float('Length', digits=(10, 2)),
    }
    
template()

class supplierinfo(orm.Model):
    
    _inherit = 'product.supplierinfo'
    
    _columns = {
        'available_qty': fields.integer('Available quantity')
    }
supplierinfo()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
