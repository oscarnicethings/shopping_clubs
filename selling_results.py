# -*- coding: utf-8 -*-
########################
# Created on 9/9/2014
#
# @author: openerp
########################

import openerp
import logging

from openerp import netsvc, tools, pooler
from openerp.osv import fields,orm
from openerp.tools.translate import _

_logger = logging.getLogger(__name__)

class selling_results(orm.Model):
    '''
    Holds info about the sales of a selling campaign
    '''
    
    _name = 'selling.results'
    _columns = {
        'selling_results_line': fields.one2many('selling.results.line', 'id', 'Products'),    
    }
    
class selling_results_line(orm.Model):
    
    _name = 'selling.results.line'
    _columns = {
        'product': fields.many2many('product.product', 'product_selling_results_rel', 'selling.results.line_id', 'product_id', 'Products'),
        'qty': fields.integer('Quantity')
    }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: