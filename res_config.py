# -*- coding: utf-8 -*-
########################
# Created on Mar 26, 2015
#
# @author: openerp
########################

from openerp.osv import fields, osv

class shopping_club_config_settings(osv.osv_memory):
    '''
    classdocs
    '''
    
    _name = 'shopping.clubs.config.settings'
    _inherit = 'res.config.settings'
    
    _columns = {
        'overrun': fields.integer('Overrun', size=2)
    }
    
shopping_club_config_settings()
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: