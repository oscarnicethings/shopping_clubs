# -*- coding: utf-8 -*-
########################
# Created on 22/8/2014
#
# @author: Oscar Blanco
########################

import openerp
import logging

from openerp import netsvc, tools, pooler
from openerp.osv import fields, orm, osv
from openerp.tools.translate import _
from tools import convert

from StringIO import StringIO
from collections import OrderedDict
import csv
import os
import shutil
from tools import zipdir, reload_browser

from translator import Translator

_logger = logging.getLogger(__name__)

class campaign(orm.Model):
    '''
    Holds data of product campaigns related to one or more shopping clubs
    '''
    
    _name = 'campaign'
    
    def _get_collections(self, cr, uid, context=None):
        return (('really', 'ReallyNiceThings'),
                ('surdic', 'Surdic'),
                ('little', 'LittleNiceThings'),
                ('capsule', 'Capsule'))
        
    _columns = {
        'name': fields.char('Name', size=25),
        'ref': fields.char('Reference', size=10),
        'campaigns_clubs_rel': fields.one2many('campaigns.clubs.rel', 'id_campaign', 'Selling campaigns'),
        'campaign_products': fields.many2many('product.product', 'campaigns_products_rel', 'campaign_id', 'product_id', 'Products'),
        'editable': fields.boolean('Editable'),
        'club': fields.char('Club initials', size=4, required=False),
        'category': fields.char('Category initials', size=8, required=False),
        'week': fields.char('Week', size=2, required=False),
        'year': fields.char('Year', size=2, required=False),
        'collection': fields.selection(_get_collections, 'Collection'),
        'capsule': fields.many2one('campaign.capsule', 'Capsule', ondelete="restrict")
    }
    
    _defaults = {
        'ref': lambda self,cr,uid,context={}: self.pool.get('ir.sequence').get(cr, uid, 'campaign.ref'),
        'editable': True
    }
    
    _sql_constraints = [
        ('unique_campaign_name', 'unique(name)', 'A campaign with the same name already exists'),
        ('unique_campaign_ref', 'unique(ref)', 'A campaign with the same reference already exists'),
    ]
    
    def create(self, cr, uid, vals, context=None):
        _logger.info(vals)
        if vals and all (k in vals for k in ('club', 'week', 'year', 'category')):
            _category = vals.get('category')
            _club = vals.get('club')
            _week = vals.get('week')
            _year = vals.get('year')
            
            if _category and _club and _week and _year:
                vals['name'] = '{0}_{1}_{2}_{3}'.format(_club, _category, _week, _year)
        elif 'name' not in vals:
            raise osv.except_osv(_('Error'), _('You should fill either Name field or category/club/week/year to assign a name to this campaign.'))
              
        record_id = super(campaign, self).create(cr, uid, vals, context=context)                
        products = self.read(cr, uid, record_id, ['campaign_products'])['campaign_products']
        self.pool.get('product.product').write(cr, uid, products, {'is_on_campaign': True})

        #self.write(cr, uid, record_id, {'state': 'pendent'}, context)
        return record_id
    
    def write(self, cr, uid, ids, vals, context=None):
        _logger.info(vals)
        if vals:
            this = self.browse(cr, uid, ids)
            _category = vals.get('category') if 'category' in vals else this.category
            _club = vals.get('club') if 'club' in vals else this.club
            _week = vals.get('week') if 'week' in vals else this.week
            _year = vals.get('year') if 'year' in vals else this.year

            if _category and _club and _week and _year:
                vals['name'] = '{0}_{1}_{2}_{3}'.format(_club, _category, _week, _year)
            elif not this.name and 'name' not in vals:
                raise osv.except_osv(_('Error'), _('You should fill either Name field or category/club/week/year to assign a name to this campaign.'))
            
        return super(campaign, self).write(cr, uid, ids, vals, context=context)
    
    def action_create_selling_campaign(self, cr, uid, ids, context=None):
        
        view_ref = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'shopping_clubs', 'campaign_to_sc_wizard')
        view_id = view_ref and view_ref[1] or False,
        return {
            'type': 'ir.actions.act_window',
            'name': _('Create SC'),
            'res_model': 'campaign.to.sc',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'target': 'new',
            'context': context
        }
        
    def action_upload_campaign(self, cr, uid, ids, context=None):
        
        view_ref = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'shopping_clubs', 'view_wizard_upload_campaign')
        view_id = view_ref and view_ref[1] or False,
        return {
            'type': 'ir.actions.act_window',
            'name': _('Upload campaign'),
            'res_model': 'upload.campaign',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'target': 'new',
            'context': {'campaign_id': ids[0]}
        }
        
    def action_clone_campaign(self, cr, uid, ids, context=None):
        
        view_ref = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'shopping_clubs', 'clone_campaign_wizard')
        view_id = view_ref and view_ref[1] or False,
        return {
            'type': 'ir.actions.act_window',
            'name': _('Clone campaign'),
            'res_model': 'clone.campaign',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'target': 'new',
            'context': context
        }
        
    def action_empty_campaign(self, cr, uid, ids, context=None):
        
        self.browse(cr, uid, ids).write({'campaign_products': [(5,)]})
        reload_browser()
    
    def print_details(self, cr, uid, ids, context=None):
        '''
        This function prints the sales order and mark it as sent, so that we can see more easily the next step of the workflow
        '''
        assert len(ids) == 1, 'This option should only be used for a single id at a time'
        return self.pool['report'].get_action(cr, uid, ids, 'shopping_clubs.report_campaign_details', context=context)
    
    def action_get_csv(self, cr, uid, ids, context=None):
        irattachment = self.pool.get('ir.attachment')
        this = self.browse(cr, uid, ids)
        _trasnlator = Translator()
        """
        rows = [{'Name': product.name,
                 'Product code': product.product_code,
                 'EAN': product.ean13,
                 'Description': product.description,
                 'PVP': product.lst_price,
                 'PVD': product.campaign_price 
                 } for product in this.campaign_products]
        """
        rows = []
        for product in this.campaign_products:
            _values = OrderedDict()
            _values['Product code'] = product.product_code
            _values['Internal reference'] = product.default_code
            _values['Name'] = product.name
            _values['Name en'] = _trasnlator.translate(product.name.encode('utf-8'))
            _values['EAN'] = product.ean13
            _values['Description'] = product.description
            _values['Description en'] = product.description_en
            _values['PVP'] = '{0}'.format(product.lst_price).replace('.', ',')
            _values['PVD'] = '{0}'.format(product.campaign_price).replace('.', ',')
            _values['Height'] = product.height
            _values['Width'] = product.width
            _values['Length'] = product.length
            _values['Packing height'] = product.packing_height
            _values['Packing width'] = product.packing_width
            _values['Packing length'] = product.packing_length
            _values['Weight'] = product.weight
            rows.append(_values)
        
        cso = StringIO()
        csv_writer = csv.DictWriter(cso, rows[0].keys(), delimiter=';')
        csv_writer.writeheader()
        csv_writer.writerows(convert(rows))
        
        ir_id = irattachment.search(cr, uid, [('res_model', '=', 'campaign'), ('res_id', '=', ids[0]), ('name', 'like', '%.csv')])
        if ir_id:
            irattachment.write(cr, uid, ir_id, {'db_datas': cso.getvalue().encode('base64')})
        else:
            ir_id = irattachment.create(cr, uid, 
                                        {'name': '{0}.csv'.format(this.name),
                                         'datas_fname': '{0}.csv'.format(this.name),
                                         'db_datas': cso.getvalue().encode('base64'),
                                         'type': 'binary',
                                         'res_model': 'campaign',
                                         'res_id': ids[0]})
        
        return {
                'name'     : 'Download',
                'res_model': 'ir.actions.act_url',
                'type'     : 'ir.actions.act_url',
                'target'   : 'current',
                'url'      : 'web/binary/saveas?model=ir.attachment&field=datas&filename_field=name&id={0}'.format(ir_id[0] if isinstance(ir_id, list) else ir_id)
               }

    def action_download_imgs(self, cr, uid, ids, context=None):
        _p_obj = self.pool.get('product.product')
        _p_ids = (self.read(cr, uid, ids, ['campaign_products'])[0]).get('campaign_products')
        if not os.path.exists('/var/tmp/tozip'):
            os.makedirs('/var/tmp/tozip')
        for _p_id in _p_ids:
            _p_info = _p_obj.read(cr, uid, _p_id, ['image', 'product_code'])
            _img = _p_info['image']
            _code = _p_info['product_code']
            if _img:
                with open('/var/tmp/tozip/{0}.jpg'.format(_code), 'w+') as img:
                    img.write(_img.decode('base64'))
        
            _product_images = _p_obj.browse(cr, uid, _p_id).product_images
            if _product_images:
                for _p_image in _product_images:
                    with open('/var/tmp/tozip/{0}'.format(_p_image.filename), 'w+') as img:
                        img.write(_p_image.image.decode('base64'))
                        
        _campaign_name = (self.read(cr, uid, ids, ['name'])[0]).get('name')
        zippath = zipdir('{0}'.format(_campaign_name.encode('utf-8')), '/var/tmp/tozip')
        with open(zippath, 'rb') as fff:
            attachment_csv = fff.read().encode('base64')

        try: 
            os.remove(zippath)
            shutil.rmtree('/var/tmp/tozip')
        except OSError:
            pass
        
        irattachment = self.pool.get('ir.attachment')
        ir_id = irattachment.search(cr, uid, [('res_model', '=', 'campaign'), ('res_id', '=', ids[0]), ('name', 'like', '%.zip')])
        if ir_id:
            irattachment.write(cr, uid, ir_id, {'db_datas': attachment_csv})
        else:
            ir_id = self.pool.get('ir.attachment').create(cr, uid, 
                                                  {'name': zippath.replace('/var/tmp/', ''),
                                                   'datas_fname': zippath.replace('/var/tmp/', ''),
                                                   'db_datas': attachment_csv,
                                                   'type': 'binary',
                                                   'res_model': 'campaign',
                                                   'res_id': ids[0]})  
        return {
                'name'     : 'Download',
                'res_model': 'ir.actions.act_url',
                'type'     : 'ir.actions.act_url',
                'target'   : 'current',
                'url'      : 'web/binary/saveas?model=ir.attachment&field=datas&filename_field=name&id={0}'.format(ir_id[0] if isinstance(ir_id, list) else ir_id)
        }

class campaign_capsule(orm.Model):

    _name = 'campaign.capsule'
    
    _columns = {
        'name': fields.char('Name', size=30),
    }
    
campaign_capsule()

class clone_campaign(osv.osv_memory):
    
    _name = 'clone.campaign'
    
    _columns = {
        'new_name': fields.char('Name', size=25, required=True),
        'state': fields.selection([('init', 'Init'), ('clone', 'Clone')]),
    }
    
    _defaults = {
        'state': 'init'
    }
    
    def action_clone(self, cr, uid, ids, context=None):
        if context.get('active_model') == 'campaign':
            _c_obj = self.pool.get('campaign')
            _c_products = _c_obj.read(cr, uid, context.get('active_id'), ['campaign_products'])['campaign_products']
            
            _c_id = _c_obj.create(cr, uid, {'name': context.get('new_name'),
                                            'campaign_products': [(6, 0, _c_products)],
                                            'editable': True})
            
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'campaign',
                'view_mode': 'form',
                'res_id': _c_id
            }
        
        raise osv.except_osv(
                    _('Error!'),
                    _('Ups! Something went wrong, please blame Oscar until this is fixed.'))
clone_campaign()

class campaign_to_sc(osv.osv_memory):
    
    _name = 'campaign.to.sc'
    
    _columns = {
        'name': fields.char('Name', size=25),
        'date_ini': fields.date('Starting date', required=True),
        'date_end': fields.date('Ending date', required=True),
        'club': fields.many2one('res.partner', 'Shopping club', domain="[('is_shopping_club','=',True)]", ondelete='restrict', required=True),
    }

    def action_create_sc(self, cr, uid, ids, context=None):
        this = self.browse(cr, uid, ids)
        _logger.info(context)
        _logger.info(ids)
        if context.get('active_model') == 'campaign':
            ccrel = self.pool.get('campaigns.clubs.rel')
            
            res_id = ccrel.create(cr, uid,
                                  {'name': this.name,
                                   'id_club': this.club.id,
                                   'date_ini': this.date_ini,
                                   'date_end': this.date_end,
                                   'id_campaign': context.get('active_id')})
           
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'campaigns.clubs.rel',
                'view_mode': 'form',
                'res_id': res_id
            }
        
        raise osv.except_osv(
                    _('Error!'),
                    _('Ups! Something went wrong, please blame Oscar until this is fixed.'))
clone_campaign()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: