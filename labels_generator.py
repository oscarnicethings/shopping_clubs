# -*- coding: utf-8 -*-
########################
# Created on Mar 10, 2015
#
# @author: openerp
########################

import openerp
import logging

from openerp import netsvc, tools, pooler
from openerp.osv import fields,orm
from openerp.tools.translate import _

_logger = logging.getLogger(__name__)

class labels_generator(orm.TransientModel):
    '''
    classdocs
    '''
    
    _name = 'labels.generator'
    
    _columns = {
        'qty': fields.integer('Quantity'),
        'product': fields.many2one('product.product', 'name', 'Product'),
        'state': fields.selection([('init', 'init'), ('generate', 'generate')]),
        'purchase_order': fields.char('Purchase order', size=25),
        'campaign_number': fields.char('Campaign number', size=25)
    }
    
    _defaults = {
        'qty': 1,
        'state': 'init'
    }
    
    def action_generate(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'product':context.get('product')})
        return self.pool['report'].get_action(cr, uid, ids, 'shopping_clubs.report_sp_product_label', context=context)
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: