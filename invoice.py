# -*- coding: utf-8 -*-
########################
# Created on 10/9/2014
#
# @author: openerp
########################

import openerp
import logging

from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning

import time
from datetime import date, timedelta

_logger = logging.getLogger(__name__)

class invoice(models.Model):
    '''
    classdocs
    '''
    _name = 'account.invoice'
    _inherit = 'account.invoice'
    
    
    ref = fields.Char(string='Reference', size=10,
        default=lambda self: self.env['ir.sequence'].get('invoice.ref'))
    custom_campaign_num = fields.Char(string='Campaign number', size=15)
    
    forecast_date = fields.Date()
    
    origin = fields.Char(string='Source Document',
        help="Reference of the document that produced this invoice.",
        readonly=True, required=True, states={'draft': [('readonly', False)]})
    
    with_irpf = fields.Boolean('With IRPF', related='partner_id.has_irpf', store=True)
    
    @api.v7
    def alert_next_due_invoices(self, cr, uid, ids=None, context=None):
        
        #today = time.strftime('%Y-%m-%d')
        today = date.today()
        next_day = today + timedelta(days=7)
        message = _('<br />Facturas que vencen la proxima semana:<br />')
        header = _('<span style="margin-left:5em">Name</span><span style="margin-left:5em">Ref.</span><span style="margin-left:5em">Nº</span><span style="margin-left:5em">Importe</span><br />')
        out_invoices_message = _('Facturas de cliente\n_______________<br />' + header)
        in_invoices_message = _('<br /><br />_______________Facturas de proveedor<br/>_______________<br />' + header)
        
        ids = self.search(cr, uid, [('date_due', '>=', today), ('date_due', '<', next_day)])
        records = self.browse(cr, uid, ids)
        for record in records:
            _msg = """
                    <span style="margin-left:5em">{0}</span>
                    <span style="margin-left:5em">{1}</span>
                    <span style="margin-left:5em">{2}</span>
                    <span style="margin-left:5em">{3}</span>
                    <span style="margin-left:5em">{4} eur.</span><br />
                    """.format(record.partner_id.name.encode('utf-8'),
                               record.ref,
                               record.number,
                               record.date_due,
                               record.amount_total)

            if record.type == 'out_invoice':
                out_invoices_message += _msg
            elif record.type == 'in_inovice':
                in_invoices_message += _msg
        
        _body = message + out_invoices_message + in_invoices_message
        msg = {'type': 'email',
               'email_from': 'info@reallynicethings.es', #uid,
               'partner_ids': [(6, 0, [14, 15, 204])],
               'body': _body,
               'subject': 'Vencimiento de facturas'}
        self.pool.get('mail.mail').create(cr, uid, msg, context)
        return
   
invoice()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: