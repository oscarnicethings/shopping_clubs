# -*- coding: utf-8 -*-
########################
# Created on 10/9/2014
#
# @author: openerp
########################

import openerp
import logging

from openerp import netsvc, tools, pooler
from openerp.osv import fields,orm,osv
from openerp.tools.translate import _

import csv
import base64
from StringIO import StringIO

from csv_mapping import sales_positions

_logger = logging.getLogger(__name__)


class upload_campaign(osv.osv_memory):
    '''
    Upload a file with campaign
    '''
        
    _name = 'upload.campaign'
    _columns = {
        'campaign_file': fields.binary('Campaign file'),
        'separator': fields.selection(((',', ','),
                                       (';', ';'),
                                       ('|', '|')), 'Separator'),
        'reference_type': fields.selection((('default_code', 'Old reference'),
                                            ('product_code', 'New reference')), 'Reference type'),
        'skip_first': fields.boolean('Skip first line'),
        'state': fields.selection([('init', 'init'), ('warning', 'warning'), ('finish', 'finish')]),
        'message': fields.text('Warning:', readonly=True)
    }
    
    _defaults = {
        'state': 'init'
    }
    
    def upload_campaign_file(self, cr, uid, ids, context=None):
        res = self.read(cr, uid, ids,['campaign_file', 'separator', 'reference_type', 'skip_first'])
        res = res and res[0] or {}
        campaign_id = context.get('campaign_id')
        
        product_ids = []
        product_obj = self.pool.get('product.product')
        
        _warning = ''
        _skip = res['skip_first']
        for row in StringIO(base64.decodestring(res['campaign_file'])):
            if _skip == True:
                _skip = False
                continue
            fields = row.split(res['separator'].encode('utf-8'))
            """
            if len(fields) == 1:
                raise osv.except_osv(
                    _('Error!'),
                    _('Check field delimiter on file. It must be (|) or (;)'))
            """
            #p_id = product_obj.search(cr, uid, [('default_code','=', fields[sales_positions['supplier_ref']])])
            p_id = product_obj.search(cr, uid, [(res['reference_type'].encode('utf-8'),'=', fields[0])])         
            if p_id and p_id[0] not in product_ids:
                product_ids.append(p_id[0])
            elif len(p_id) == 0:
                _warning = _warning + 'El producto con {0} {1} no se ha encontrado en la base de datos, revisa la informacion.\n'.format(res['reference_type'].encode('utf-8'), fields[0])
            
        if len(product_ids) > 0:
            self.pool.get('campaign').write(cr, uid, campaign_id, {'campaign_products': [(6, 0, product_ids)]})
        
        if _warning:
            self.write(cr, uid, ids[0], {'state': 'warning', 'message':_warning}, context=context)
            # return view
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'upload.campaign',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': ids[0],
                'views': [(False, 'form')],
                'target': 'new',
                'context': context
            }
        
upload_campaign()